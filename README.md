#weixin.MP.SDK


示例代码在src/example下


联系方式:
电子邮箱:zhzhbin1030@163.com

多客服接口中的PC客户端自定义插件接口 - 这个好像没得做API..<br>
加密解密接口 - 因需要从orcale下载jar替换本地jdk中的jar包后才能使用该接口,我实在不想去搞...<br><br>
语义解析接口 - 接口内容太多,暂时没心情做 <br>
微小店接口 - 接口内容太多,暂时没心情做<br>
设备功能接口 - 我不做设备开发,完全没想法<br>

<br>
以上接口尚未实现

微信公众平台Java SDK
==============



## 介绍
weixin.MP.SDK 提供便捷的API调用接口.
config.properties

--该配置文件中是微信的相关参数配置，前两个参数感觉项目需要修改

defaultEmailServer.properties

--部分服务器地址解析时自动补全smtp和imap地址的配置

email.properties

--邮箱配置


多客服接口中的PC客户端自定义插件接口 - 这个好像没得做API..<br>
加密解密接口 - 因需要从orcale下载jar替换本地jdk中的jar包后才能使用该接口,我实在不想去搞...<br>
语义解析接口 - 接口内容太多,暂时没心情做 <br>
微小店接口 - 接口内容太多,暂时没心情做<br>
设备功能接口 - 我不做设备开发,完全没想法<br>

以上接口尚未实现<br>

没有申请支付账户,支付接口只是引入了官方的sdk<br>

已实现功能<br>
基础支持-获取accessToken WeiXinUtil.getAccessToken() 或 WeiXinService.getAccessToken(); 需将appid和appSecret填写到config.properties<br>
基础支持-获取微信服务器IP地址WeiXinService.getIPList(accessToken)<br>
基础支持-上传下载多媒体文件 WeiXinService.uploadMedia(file,type,accessToken);WeiXinService.downloadMedia(filepath, mediaId,accessToken);<br>
接收消息-验证消息真实性 参考com.zzb.weixin.servlet,doGet方法做验证,doPost方法做微信推送消息处理<br>
接收消息-接收普通消息 参考com.zzb.weixin.servlet doPost接收消息<br>
接收消息-接收事件推送消息 参考com.zzb.weixin.servlet doPost接收消息<br>
接收消息-接收语音识别消息 参考com.zzb.weixin.servlet doPost接收消息<br>
<br>
发送消息-发送被动响应消息 参考com.zzb.weixin.servlet doPost接收消息<br>
发送消息-发送客服消息 参考example.Weixin sendCustonMsg<br>
发送消息-高级群发消息 WeiXinService.uploadSendNews(accessToken, List<Article>) WeiXinService.uploadSendVideo() WeiXinService.send()<br>
<br>
用户管理-分组管理 创建分组WeiXinService.createGroup(String, Group) <br>
			查询所有分组WeiXinService.getGroups(String)<br>
			修改分组名称WeiXinService.updateGroup(String, Group)<br> 
			查询用户所在分组WeiXinService.getGroupByOpenID(String, String) <br>
			移动用户分组WeiXinService.updateMemberGroup(String, String, String)<br>
用户管理-设置用户备注名 WeiXinService.updateRemark(String, String, String)<br>
用户管理-获取用户基本信息  WeiXinService.getUserInfo(String, String, WXLang)<br>
用户管理-获取关注者列表  WeiXinService.getUserIdList(String, String)<br>
用户管理-获取用户地理位置 参见事件推送<br>
用户管理-网页授权获取用户基本信息 获取codeurl WeiXinService.getCodeUrl(String, String, WXOauthScope, String)<br>
					通过code换取网页授权access_token WeiXinService.getOauthToken(String)<br>
					刷新access_token WeiXinService.refushOauthToken(String)<br>
					拉取用户信息(需scope为 snsapi_userinfo) WeiXinService.getOauthUserInfo(String, String, WXLang)<br>
<br>
多客服功能-将消息转发到多客服	在消息响应中回复CSMsg的一个实例<br>
		获取客服聊天记录	getCSRecord(String, CSRecordPageInfo)<br>
<br>
自定义菜单 - 创建 WeiXinService.createMenu(String, Menu)<br>
		查询 WeiXinService.getMenu(String)<br>
		删除WeiXinService.deleteMenu(String)<br>
		时间推送 参见接收消息-事件推送<br>
<br>
推广支持 - 生成带参数二维码 WeiXinService.createQRcode(String, QRCodeInfo)<br>
			通过ticket换取二维码URl WeiXinService.getQRCodeUrl(String)<br>
			通过ticket下载二维码 WeiXinService.downloadQRCode(String)<br>
		长链接转短链接接口 WeiXinService.getShortUrl(String, String)<br>
<br>

## 参考资料
* [微信公众平台开发者文档](http://mp.weixin.qq.com/wiki/index.php)