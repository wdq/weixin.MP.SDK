package com.zzb.weixin.json;

import java.util.ArrayList;
import java.util.List;

import com.zzb.weixin.org.json.JSONArray;
import com.zzb.weixin.org.json.JSONException;

public class JsonArray {
	private JSONArray ja;

	public JsonArray() {
		setJa(new JSONArray());
	}

	public void add(JsonObject jsonObject) {
		getJa().put(jsonObject.getJo());
	}
	public void add(String string) {
		getJa().put(string);
	}

	public JSONArray getJa() {
		return ja;
	}

	public List<String> getAsStringList() {
		List<String> list = new ArrayList<String>();
		for (int i=0;i<getJa().length();i++) {
			list.add(getJa().optString(i));
		}
		return list;
	}

	public List<JsonObject> getAsJsonObjectList() {
		List<JsonObject> list = new ArrayList<JsonObject>();
		for (int i=0;i<getJa().length();i++) {
			JsonObject jo = new JsonObject();
			jo.setJo(getJa().optJSONObject(i));
			list.add(jo);
		}
		return list;
	}
	public JsonObject getAsJsonObject(int index){
		try {
			return new JsonObject(getJa().getJSONObject(index));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
	public int length(){
		return getJa().length();
	}

	@Override
	public String toString() {
		return getJa().toString();
	}

	public void setJa(JSONArray ja) {
		this.ja = ja;
	}
}
