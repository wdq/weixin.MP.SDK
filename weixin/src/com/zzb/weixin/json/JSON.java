package com.zzb.weixin.json;

import com.zzb.weixin.org.json.JSONException;
import com.zzb.weixin.org.json.JSONObject;


public class JSON {
	public static JsonObject parse(String s){
		try {
			return new JsonObject(new JSONObject(s));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}
}
