package com.zzb.weixin.model.user;

import com.zzb.util.StringUtil;
import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;

public class Group {
	private String id;
	private String name;
	private int count;

	public Group(JsonObject jo) {
		id = jo.getAsString(WXJsonName.ID);
		name = jo.getAsString(WXJsonName.NAME);
		count = jo.getAsInt(WXJsonName.COUNT);
	}

	public Group() {
	}

	public JsonObject toJson() {
		JsonObject jo = new JsonObject();
		if(!StringUtil.isEmpty(id))jo.put(WXJsonName.ID, id);
		jo.put(WXJsonName.NAME, name);
		JsonObject group = new JsonObject();
		group.put(WXJsonName.GROUP, jo);
		return group;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}
