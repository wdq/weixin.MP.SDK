package com.zzb.weixin.model.user;

import java.util.List;

import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonArray;
import com.zzb.weixin.json.JsonObject;

public class WeiXinUserIdList {
	private Integer total;
	private int count;
	private List<String> data;
	private String nextOpenId;

	public WeiXinUserIdList(JsonObject jo) {
		this.setTotal(jo.getAsInt(WXJsonName.TOTAL));
		if (this.getTotal() != null) {
			this.setCount(jo.getAsInt(WXJsonName.COUNT));
			this.setNextOpenId(jo.getAsString(WXJsonName.NEXT_OPENID));
			JsonArray ja = jo.getAsJsonObject(WXJsonName.DATA).getAsJsonArray(
					WXJsonName.OPEN_ID);
			this.setData(ja.getAsStringList());
		}
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<String> getData() {
		return data;
	}

	public void setData(List<String> data) {
		this.data = data;
	}

	public String getNextOpenId() {
		return nextOpenId;
	}

	public void setNextOpenId(String nextOpenId) {
		this.nextOpenId = nextOpenId;
	}
}
