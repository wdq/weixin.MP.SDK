package com.zzb.weixin.model.user;

import java.util.List;

import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.constant.Constant.WXLang;
import com.zzb.weixin.json.JsonArray;
import com.zzb.weixin.json.JsonObject;

public class WeiXinUser {
	private String subscribe;//用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。
	private String openId;//用户的标识，对当前公众号唯一
	private String nickName;//用户的昵称
	private String sex;//用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
	private WXLang language;//用户的语言，简体中文为zh_CN
	private String city;//用户所在城市
	private String province;//用户所在省份
	private String country;//用户所在国家
	private String headImgUrl;//用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空
	private String subscribeTime;//用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
	private String unionid;//只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
	private List<String> privilege;//用户特权信息，json 数组，如微信沃卡用户为（chinaunicom）
	public WeiXinUser() {
	}
	public WeiXinUser(JsonObject jo) {
		this.setSubscribe(jo.getAsString(WXJsonName.SUBSCRIBE));
		this.setOpenId(jo.getAsString(WXJsonName.OPEN_ID));
		this.setNickName(jo.getAsString(WXJsonName.NICK_NAME));
		this.setSex(jo.getAsString(WXJsonName.SEX));
		this.setLanguage(WXLang.valueOf(jo.getAsString(WXJsonName.LANGUAGE)));
		this.setCity(jo.getAsString(WXJsonName.CITY));
		this.setProvince(jo.getAsString(WXJsonName.PROVINCE));
		this.setCountry(jo.getAsString(WXJsonName.COUNTRY));
		this.setHeadImgUrl(jo.getAsString(WXJsonName.HEAD_IMG_URL));
		this.setSubscribeTime(jo.getAsString(WXJsonName.SUBSCRIBE_TIME));
		this.setUnionid(jo.getAsString(WXJsonName.UNIONID));
		JsonArray ja = jo.getAsJsonArray(WXJsonName.PRIVILEGE);
		if(ja!=null){
			privilege = ja.getAsStringList();
		}
	}
	public String getSubscribe() {
		return subscribe;
	}
	public void setSubscribe(String subscribe) {
		this.subscribe = subscribe;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public WXLang getLanguage() {
		return language;
	}
	public void setLanguage(WXLang language) {
		this.language = language;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getHeadImgUrl() {
		return headImgUrl;
	}
	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}
	public String getSubscribeTime() {
		return subscribeTime;
	}
	public void setSubscribeTime(String subscribeTime) {
		this.subscribeTime = subscribeTime;
	}
	public List<String> getPrivilege() {
		return privilege;
	}
	public void setPrivilege(List<String> privilege) {
		this.privilege = privilege;
	}
	public String getUnionid() {
		return unionid;
	}
	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}
}
