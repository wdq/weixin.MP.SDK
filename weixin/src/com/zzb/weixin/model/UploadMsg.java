package com.zzb.weixin.model;

import com.zzb.weixin.constant.Constant.WXMediaType;
import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;
/**
 * 上传媒体文件响应,
 * 媒体文件在后台保存时间为3天，即3天后media_id失效。
 * @author zb.zhang
 *
 */
public class UploadMsg {
	private WXMediaType type ;
	private String mediaId;//媒体文件在后台保存时间为3天，即3天后media_id失效。
	private String createdAt;
	public UploadMsg() {
	}
	public UploadMsg(JsonObject jo) {
		this.setCreatedAt(jo.getAsString(WXJsonName.CREATED_AT));
		this.setType(WXMediaType.valueOf(jo.getAsString(WXJsonName.TYPE)));
		this.setMediaId(jo.getAsString(WXJsonName.MEDIA_ID));
	}
	public WXMediaType getType() {
		return type;
	}
	public void setType(WXMediaType type) {
		this.type = type;
	}
	public String getMediaId() {
		return mediaId;
	}
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
}
