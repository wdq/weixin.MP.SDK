package com.zzb.weixin.model.msg.send;

import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;

public class GroupSendMsg extends SendMsg {
	private String groupId;

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	@Override
	public JsonObject toJson() {
		JsonObject sendMsg = super.toJson();
		//构造消息发送条件
		JsonObject jo = new JsonObject();
		jo.put(WXJsonName.GROUP_ID, groupId);
		sendMsg.put(WXJsonName.FILTER, jo);
		return sendMsg;
	}
}
