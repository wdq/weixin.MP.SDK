package com.zzb.weixin.model.msg.send;

import com.zzb.weixin.constant.Constant.WXSendMsgType;
import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;

public abstract class SendMsg {
	private WXSendMsgType type;
	private String mediaId;
	private String content;

	public JsonObject toJson() {
		JsonObject sendMsg = new JsonObject();
		// 构造消息内容
		JsonObject jo = new JsonObject();
		sendMsg.put(WXJsonName.TYPE, getType().toString());
		switch (getType()) {
		case text:
			jo.put(WXJsonName.CONTENT, getContent());
			break;
		default:
			jo.put(WXJsonName.MEDIA_ID, getMediaId());
			break;
		}
		sendMsg.put(getType().toString(), jo);
		return sendMsg;
	}

	public WXSendMsgType getType() {
		return type;
	}

	public void setType(WXSendMsgType type) {
		this.type = type;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
