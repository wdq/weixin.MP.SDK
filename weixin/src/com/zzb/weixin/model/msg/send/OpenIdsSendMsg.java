package com.zzb.weixin.model.msg.send;

import java.util.List;

import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonArray;
import com.zzb.weixin.json.JsonObject;

public class OpenIdsSendMsg extends SendMsg {
	private List<String> openids;

	@Override
	public JsonObject toJson() {
		JsonObject sendMsg = super.toJson();
		//构造消息发送条件
		JsonObject jo = new JsonObject();
		JsonArray ja = new JsonArray();
		for (String openid : openids) {
			ja.add(openid);
		}
		jo.put(WXJsonName.TOUSER, ja);
		sendMsg.put(WXJsonName.FILTER, jo);
		return sendMsg;
	}

	public List<String> getOpenids() {
		return openids;
	}

	public void setOpenids(List<String> openids) {
		this.openids = openids;
	}
}
