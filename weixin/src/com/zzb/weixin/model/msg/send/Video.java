package com.zzb.weixin.model.msg.send;

import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;
/**
 * 上传视频供群发消息使用
 * @author zb.zhang
 *
 */
public class Video {
	private String title;//
	private String description;//
	private String media_id;//此处media_id需通过基础支持中的上传下载多媒体文件来得到
	public JsonObject toJson(){
		JsonObject jsonObject = new JsonObject();
		jsonObject.put(WXJsonName.DESCRIPTION, description);
		jsonObject.put(WXJsonName.TITLE, title);
		jsonObject.put(WXJsonName.MEDIA_ID, media_id);
		return jsonObject;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMedia_id() {
		return media_id;
	}
	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}
}
