package com.zzb.weixin.model.msg.send;

import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;

public class SendResultMsg {

	private String errCode ;
	private String errMsg;
	private String msgId;
	public SendResultMsg(JsonObject jo) {
		errCode = jo.getAsString(WXJsonName.ERR_CODE);
		errMsg = jo.getAsString(WXJsonName.ERR_MSG);
		msgId = jo.getAsString(WXJsonName.MSG_ID);
	}
	public String getErrCode() {
		return errCode;
	}
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
}
