package com.zzb.weixin.model.msg.send;

import com.zzb.util.StringUtil;
import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;
/**
 * 上传图文信息供群发消息使用
 * @author zb.zhang
 *
 */
public class Article {
	private String thumbMediaId;//	是	图文消息缩略图的media_id，可以在基础支持-上传多媒体文件接口中获得
	private String author;//	否	图文消息的作者
	private String title;//	是	图文消息的标题
	private String contentSourceUrl;//	否	在图文消息页面点击“阅读原文”后的页面
	private String content;//	是	图文消息页面的内容，支持HTML标签
	private String digest;//	否	图文消息的描述
	private String showCoverPic;//	否	是否显示封面，1为显示，0为不显示
	public JsonObject toJson(){
		JsonObject jsonObject = new JsonObject();
		jsonObject.put(WXJsonName.THUMB_MEDIA_ID, thumbMediaId);
		if(!StringUtil.isEmpty(author))jsonObject.put(WXJsonName.AUTHOR, author);
		jsonObject.put(WXJsonName.TITLE, title);
		if(!StringUtil.isEmpty(contentSourceUrl))jsonObject.put(WXJsonName.CONTENT_SOURCE_URL, contentSourceUrl);
		jsonObject.put(WXJsonName.CONTENT, content);
		if(!StringUtil.isEmpty(digest))jsonObject.put(WXJsonName.DIGEST, digest);
		if(!StringUtil.isEmpty(showCoverPic))jsonObject.put(WXJsonName.SHOW_COVER_PIC, showCoverPic);
		return jsonObject;
	}
	public String getThumbMediaId() {
		return thumbMediaId;
	}
	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContentSourceUrl() {
		return contentSourceUrl;
	}
	public void setContentSourceUrl(String contentSourceUrl) {
		this.contentSourceUrl = contentSourceUrl;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getDigest() {
		return digest;
	}
	public void setDigest(String digest) {
		this.digest = digest;
	}
	public String getShowCoverPic() {
		return showCoverPic;
	}
	public void setShowCoverPic(String showCoverPic) {
		this.showCoverPic = showCoverPic;
	}
}
