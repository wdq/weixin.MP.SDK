package com.zzb.weixin.model.msg.mcs;

import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;


/**
 * 获取客服消息条件及分页
 * @author user
 *
 */
public class CSRecordPageInfo {
	private String startTime;//查询开始时间，UNIX时间戳(必填)
	private String endTime;//查询结束时间，UNIX时间戳，每次查询不能跨日查询(必填)
	private String openId;//普通用户的标识，对当前公众号唯一(非必填)
	private int pageSize;//每页大小，每页最多拉取1000条(必填)
	private int pageIndex;//查询第几页，从1开始(必填)
	
	public JsonObject toJson(){
		JsonObject jo = new JsonObject();
		jo.put(WXJsonName.START_TIME, startTime);
		jo.put(WXJsonName.END_TIME, endTime);
		jo.put(WXJsonName.OPEN_ID, openId);
		jo.put(WXJsonName.PAGE_SIZE, pageSize);
		jo.put(WXJsonName.PAGE_INDEX, pageIndex);
		return jo;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageIndex() {
		return pageIndex;
	}
	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}
}
