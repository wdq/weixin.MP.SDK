package com.zzb.weixin.model.msg.mcs;

import org.w3c.dom.Element;

import com.zzb.weixin.constant.Constant.WXMsgType;
import com.zzb.weixin.json.JsonObject;
import com.zzb.weixin.model.msg.basic.HeadMsg;
import com.zzb.weixin.model.msg.basic.Msg;
/**
 * 将会话转发到多客服
 * @author user
 *
 */
public class CSMsg extends Msg {
	public CSMsg() {
		this(new HeadMsg());
	}
	public CSMsg(HeadMsg head) {
		this.head.setMsgType(WXMsgType.transfer_customer_service);
	}
	@Override
	public Element write() {
		return head.write();
	}
	
	/**
	 * 该消息不接收,不实现
	 */
	@Override
	public void read() {
		
	}

	@Override
	/**
	 * 该消息不发送客服消息,不实现
	 */
	public JsonObject toJson() {
		return null;
	}
	
}
