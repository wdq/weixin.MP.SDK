package com.zzb.weixin.model.msg.mcs;

import com.zzb.weixin.constant.Constant;
import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;

public class CSResponseInfo {
	private String worker;//客服账号
	private String openid;//用户的标识，对当前公众号唯一
	private String operCode;//操作ID（会话状态），具体说明见下文
	private long time;//操作时间，UNIX时间戳
	private String text;//聊天记录
	public CSResponseInfo(JsonObject jo) {
		this.worker = jo.getAsString(WXJsonName.WORKER);
		this.openid = jo.getAsString(WXJsonName.OPEN_ID);
		this.operCode = jo.getAsString(WXJsonName.OPER_CODE);
		this.time = jo.getAsLong(WXJsonName.TIME);
		this.text = jo.getAsString(WXJsonName.TEXT);
	}
	public String getWorker() {
		return worker;
	}
	public void setWorker(String worker) {
		this.worker = worker;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getOperCode() {
		return operCode;
	}
	public String getOperMsg(){
		return Constant.operCodeMap.get(operCode);
	}
	public void setOperCode(String operCode) {
		this.operCode = operCode;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
}
