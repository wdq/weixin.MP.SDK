package com.zzb.weixin.model.msg.template;

import java.util.List;

import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;

public class TemplateMsg {
	private String touser;//消息接收人openid
	private String templateId;//模板id
	private String url;//
	private String topcolor;
	private List<KeyNote> data;
	public JsonObject toJson(){
		JsonObject jo = new JsonObject();
		jo.put(WXJsonName.TO_USER, touser);
		jo.put(WXJsonName.URL, url);
		jo.put(WXJsonName.TEMPLATE_ID, templateId);
		jo.put(WXJsonName.TOP_COLOR, topcolor);
		JsonObject dataObject = new JsonObject();
		for (KeyNote keynote : data) {
			dataObject.put(keynote.getName(),keynote.toJson());
		}
		jo.put(WXJsonName.DATA, dataObject);
		return jo;
	}
	public String getTouser() {
		return touser;
	}
	public void setTouser(String touser) {
		this.touser = touser;
	}
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTopcolor() {
		return topcolor;
	}
	public void setTopcolor(String topcolor) {
		this.topcolor = topcolor;
	}
	public List<KeyNote> getData() {
		return data;
	}
	public void setData(List<KeyNote> data) {
		this.data = data;
	}
}
