package com.zzb.weixin.model.msg.template;

import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;

public class KeyNote {
	private String name;
	private String value;
	private String color;
	public JsonObject toJson(){
		JsonObject jo = new JsonObject();
		jo.put(WXJsonName.VALUE, value);
		jo.put(WXJsonName.COLOR, value);
		return jo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
}
