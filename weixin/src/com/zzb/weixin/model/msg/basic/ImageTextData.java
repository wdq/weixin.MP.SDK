package com.zzb.weixin.model.msg.basic;

import org.w3c.dom.Element;

import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.json.JsonObject;

/**
 * 图文消息对象 为图文消息对象提供数据支持
 * 
 * @author zzb
 */
public class ImageTextData extends Msg {

	// 图文消息标题
	private String title;
	// 图文消息描述
	private String description;
	// 图片链接，支持JPG、PNG格式，较好的效果为大图640*320，小图80*80。
	private String picUrl;
	// 点击图文消息跳转链接
	private String url;

	/**
	 * 所以属性的构造方法
	 * 
	 * @param title
	 *            标题
	 * @param description
	 *            描述
	 * @param picUrl
	 *            图片地址
	 * @param url
	 *            url地址
	 */
	public ImageTextData(String title, String description, String picUrl,
			String url) {
		this.title = title;
		this.description = description;
		this.picUrl = picUrl;
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public Element write() {
		Element itemElement = document.createElement(WXXmlElementName.ITEM);
		Element titleElement = document.createElement(WXXmlElementName.TITLE);
		titleElement.setTextContent(this.getTitle());
		Element descriptionElement = document
				.createElement(WXXmlElementName.DESCRIPTION);
		descriptionElement.setTextContent(this.getDescription());
		Element picUrlElement = document
				.createElement(WXXmlElementName.PIC_URL);
		picUrlElement.setTextContent(this.getPicUrl());
		Element urlElement = document.createElement(WXXmlElementName.URL);
		urlElement.setTextContent(this.getUrl());
		itemElement.appendChild(titleElement);
		itemElement.appendChild(descriptionElement);
		itemElement.appendChild(picUrlElement);
		itemElement.appendChild(urlElement);
		return itemElement;
	}

	@Override
	public void read() {

	}

	@Override
	public JsonObject toJson() {
		JsonObject con = new JsonObject();
		con.put(WXJsonName.TITLE, title);
		con.put(WXJsonName.DESCRIPTION, description);
		con.put(WXJsonName.URL, url);
		con.put(WXJsonName.PIC_URL, picUrl);
		return con;
	}

}
