package com.zzb.weixin.model.msg.basic;

import org.w3c.dom.Element;

import com.zzb.weixin.constant.Constant.WXMsgType;
import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.json.JsonObject;

/**
 * 地理位置消息（只能接收）
 * 
 * @author zzb
 */
public class LocationMsg extends Msg {
	// 地理位置纬度
	private String location_X;
	// 地理位置经度
	private String location_Y;
	// 地图缩放大小
	private String scale;
	// 地理位置信息
	private String label;
	// 消息id，64位整型
	private String msgId;

	/**
	 * 开发者调用
	 * */
	public LocationMsg() {
		this(new HeadMsg());
	}

	/**
	 * 程序内部调用
	 * */
	public LocationMsg(HeadMsg head) {
		this.head = head;
		this.head.setMsgType(WXMsgType.location);
	}

	@Override
	public Element write() {
		return null;
	}

	@Override
	public void read() {
		this.location_X = getElementContent(WXXmlElementName.LOCATION_X);
		this.location_Y = getElementContent(WXXmlElementName.LOCATION_Y);
		this.scale = getElementContent(WXXmlElementName.SCALE);
		this.label = getElementContent(WXXmlElementName.LABEL);
		this.msgId = getElementContent(WXXmlElementName.MSG_ID);
	}

	public String getLocation_X() {
		return location_X;
	}

	public void setLocation_X(String location_X) {
		this.location_X = location_X;
	}

	public String getLocation_Y() {
		return location_Y;
	}

	public void setLocation_Y(String location_Y) {
		this.location_Y = location_Y;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	@Override
	public JsonObject toJson() {
		// TODO 自动生成的方法存根
		return null;
	}
}
