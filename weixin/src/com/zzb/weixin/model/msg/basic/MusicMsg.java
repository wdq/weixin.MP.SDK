package com.zzb.weixin.model.msg.basic;

import org.w3c.dom.Element;

import com.zzb.weixin.constant.Constant.WXMsgType;
import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.json.JsonObject;

/**
 * 音乐消息(只能回复)
 * 
 * @author zzb
 */
public class MusicMsg extends Msg {
	// 标题
	private String title;
	// 描述
	private String description;
	// 音乐链接
	private String musicUrl;
	// 高质量音乐链接，WIFI环境优先使用该链接播放音乐
	private String hQMusicUrl;
	// 缩略图的媒体id，通过上传多媒体文件，得到的id
	private String thumbMediaId;

	/**
	 * 开发者调用
	 * */
	public MusicMsg() {
		this(new HeadMsg());
	}
	public MusicMsg(HeadMsg head) {
		this.head = new HeadMsg();
		this.head.setMsgType(WXMsgType.music);
	}

	@Override
	public Element write() {
		Element xml = head.write();
		Element musicElement = document.createElement(WXXmlElementName.MUSIC);
		Element titleElement = document.createElement(WXXmlElementName.TITLE);
		titleElement.setTextContent(this.title);
		Element descriptionElement = document.createElement(WXXmlElementName.DESCRIPTION);
		descriptionElement.setTextContent(this.description);
		Element musicUrlElement = document.createElement(WXXmlElementName.MUSIC_URL);
		musicUrlElement.setTextContent(this.musicUrl);
		Element hQMusicUrlElement = document.createElement(WXXmlElementName.HQ_MUSIC_URL);
		hQMusicUrlElement.setTextContent(this.hQMusicUrl);
		Element thumbMediaIdElement = document.createElement(WXXmlElementName.THUMBMEDIAID);
		thumbMediaIdElement.setTextContent(this.thumbMediaId);

		musicElement.appendChild(titleElement);
		musicElement.appendChild(descriptionElement);
		musicElement.appendChild(musicUrlElement);
		musicElement.appendChild(hQMusicUrlElement);
		musicElement.appendChild(thumbMediaIdElement);
		xml.appendChild(musicElement);

		return xml;
	}

	// 因为用户不能发送音乐消息给我们，因此没有实现
	@Override
	public void read() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMusicUrl() {
		return musicUrl;
	}

	public void setMusicUrl(String musicUrl) {
		this.musicUrl = musicUrl;
	}

	public String getHQMusicUrl() {
		return hQMusicUrl;
	}

	public void setHQMusicUrl(String hQMusicUrl) {
		this.hQMusicUrl = hQMusicUrl;
	}

	/**
	 * @return the hQMusicUrl
	 */
	public String gethQMusicUrl() {
		return hQMusicUrl;
	}

	/**
	 * @param hQMusicUrl
	 *            the hQMusicUrl to set
	 */
	public void sethQMusicUrl(String hQMusicUrl) {
		this.hQMusicUrl = hQMusicUrl;
	}

	/**
	 * @return the thumbMediaId
	 */
	public String getThumbMediaId() {
		return thumbMediaId;
	}

	/**
	 * @param thumbMediaId
	 *            the thumbMediaId to set
	 */
	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

	@Override
	public JsonObject toJson() {
		JsonObject con = new JsonObject();
		con.put(WXJsonName.TITLE, title);
		con.put(WXJsonName.DESCRIPTION, description);
		con.put(WXJsonName.MUSIC_URL, musicUrl);
		con.put(WXJsonName.HQ_MUSIC_URL, hQMusicUrl);
		con.put(WXJsonName.THUMB_MEDIA_ID, thumbMediaId);
		JsonObject msg = head.toJson();
		msg.put(WXJsonName.MUSIC, con);
		return msg;
	}
}
