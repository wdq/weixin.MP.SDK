package com.zzb.weixin.model.msg.basic;

import org.w3c.dom.Element;

import com.zzb.weixin.constant.Constant.WXMsgType;
import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.json.JsonObject;

/**
 * 图片消息
 * 
 * @author zzb
 */
public class ImageMsg extends Msg {

	// 图片链接
	private String picUrl;
	// 消息id，64位整型
	private String msgId;
	// 图片消息媒体id
	private String mediaId;

	/**
	 * 开发者调用
	 * */
	public ImageMsg() {
		this(new HeadMsg());
	}

	/**
	 * 程序内部调用
	 * */
	public ImageMsg(HeadMsg head) {
		this.head = head;
		this.head.setMsgType(WXMsgType.image);// 设置消息类型
	}

	@Override
	public Element write() {
		Element xml = head.write();
		Element imageElement = document.createElement(WXXmlElementName.IMAGE);
		Element mediaIdElement = document
				.createElement(WXXmlElementName.MEDIAID);
		mediaIdElement.setTextContent(mediaId);
		imageElement.appendChild(mediaIdElement);
		xml.appendChild(imageElement);
		return xml;
	}

	@Override
	public void read() {
		this.picUrl = getElementContent(WXXmlElementName.PIC_URL);
		this.mediaId = getElementContent(WXXmlElementName.MEDIAID);
		this.msgId = getElementContent(WXXmlElementName.MSG_ID);
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	/**
	 * @return the mediaId
	 */
	public String getMediaId() {
		return mediaId;
	}

	/**
	 * @param mediaId
	 *            the mediaId to set
	 */
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	@Override
	public JsonObject toJson() {
		JsonObject con = new JsonObject();
		con.put(WXJsonName.MEDIA_ID, mediaId);
		JsonObject msg = head.toJson();
		msg.put(WXJsonName.IMAGE, con);
		return msg;
	}

}
