package com.zzb.weixin.model.msg.basic;

import org.w3c.dom.Element;

import com.zzb.weixin.constant.Constant.WXMsgType;
import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.json.JsonObject;

/**
 * 视频消息
 * 
 * @author zzb
 */
public class VideoMsg extends Msg {
	// 视频消息媒体id，可以调用多媒体文件下载接口拉取数据。
	private String mediaId;
	// 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
	private String thumbMediaId;
	// 消息id，64位整型
	private String msgId;
	
	private String title;
	
	private String description;

	/**
	 * 开发者调用
	 * */
	public VideoMsg() {
		this(new HeadMsg());
	}

	/**
	 * @param head
	 */
	public VideoMsg(HeadMsg head) {
		this.head = head;
		this.head.setMsgType(WXMsgType.video);
	}

	@Override
	public Element write() {
		Element mediaIdElement = document.createElement(WXXmlElementName.MEDIAID);
		mediaIdElement.setTextContent(mediaId);
		Element titleElement = document.createElement(WXXmlElementName.TITLE);
		titleElement.setTextContent(title);
		Element descritionElement = document.createElement(WXXmlElementName.DESCRIPTION);
		descritionElement.setTextContent(description);
		
		Element videoElement = document.createElement(WXXmlElementName.VIDEO);
		videoElement.appendChild(mediaIdElement);
		videoElement.appendChild(titleElement);
		videoElement.appendChild(descritionElement);
		
		Element xml = head.write();
		xml.appendChild(videoElement);
		return xml;
	}

	// 因为用户不能发送音乐消息给我们，因此没有实现
	@Override
	public void read() {
		this.mediaId = getElementContent(WXXmlElementName.MEDIAID);
		this.thumbMediaId = getElementContent(WXXmlElementName.THUMBMEDIAID);
		this.msgId = getElementContent(WXXmlElementName.MSG_ID);
	}

	/**
	 * @return the mediaId
	 */
	public String getMediaId() {
		return mediaId;
	}

	/**
	 * @param mediaId
	 *            the mediaId to set
	 */
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	/**
	 * @return the thumbMediaId
	 */
	public String getThumbMediaId() {
		return thumbMediaId;
	}

	/**
	 * @param thumbMediaId
	 *            the thumbMediaId to set
	 */
	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

	/**
	 * @return the msgId
	 */
	public String getMsgId() {
		return msgId;
	}

	/**
	 * @param msgId
	 *            the msgId to set
	 */
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	@Override
	public JsonObject toJson() {
		JsonObject con = new JsonObject();
		con.put(WXJsonName.MEDIA_ID, mediaId);
		con.put(WXJsonName.TITLE, title);
		con.put(WXJsonName.DESCRIPTION, description);
		JsonObject msg = head.toJson();
		msg.put(WXJsonName.VIDEO, con);
		return msg;
	}
}