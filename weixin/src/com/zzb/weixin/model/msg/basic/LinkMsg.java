package com.zzb.weixin.model.msg.basic;

import org.w3c.dom.Element;

import com.zzb.weixin.constant.Constant.WXMsgType;
import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.json.JsonObject;

/**
 * 链接消息
 * 
 * @author zzb
 * */
public class LinkMsg extends Msg {

	// 消息标题
	private String title;
	// 消息描述
	private String description;
	// 消息链接
	private String url;
	// 消息id，64位整型
	private String msgId;

	/**
	 * 开发者调用创建实例
	 * */
	public LinkMsg() {
		this(new HeadMsg());
	}

	/**
	 * 推送来的消息采用此构造
	 * 
	 * @param head
	 */
	public LinkMsg(HeadMsg head) {
		this.head = head;
		this.head.setMsgType(WXMsgType.link);
	}

	@Override
	public Element write() {
		return null;
	}

	@Override
	public void read() {
		this.title = getElementContent(WXXmlElementName.TITLE);
		this.description = getElementContent(WXXmlElementName.DESCRIPTION);
		this.url = getElementContent(WXXmlElementName.URL);
		this.msgId = getElementContent(WXXmlElementName.MSG_ID);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	@Override
	public JsonObject toJson() {
		// TODO 自动生成的方法存根
		return null;
	}

}
