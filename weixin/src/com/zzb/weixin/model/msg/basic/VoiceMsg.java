package com.zzb.weixin.model.msg.basic;

import org.w3c.dom.Element;

import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.constant.Constant.WXMsgType;
import com.zzb.weixin.json.JsonObject;

/**
 * @author zzb
 */
public class VoiceMsg extends Msg {

	// 语音消息媒体id，可以调用多媒体文件下载接口拉取该媒体
	private String mediaId;
	// 语音格式：amr
	private String format;
	// 语音识别结果，UTF8编码
	private String recognition;
	// 消息id，64位整型
	private String msgId;

	/**
	 * 默认构造
	 */
	public VoiceMsg() {
		this(new HeadMsg());
	}

	public VoiceMsg(HeadMsg head) {
		this.head = head;
		this.head.setMsgType(WXMsgType.voice);
	}

	@Override
	public Element write() {
		Element mediaIdElement = document
				.createElement(WXXmlElementName.MEDIAID);
		mediaIdElement.setTextContent(mediaId);

		Element voiceElement = document.createElement(WXXmlElementName.VOICE);
		voiceElement.appendChild(mediaIdElement);

		Element xml = head.write();
		xml.appendChild(voiceElement);
		return xml;
	}

	@Override
	public void read() {
		this.mediaId = getElementContent(WXXmlElementName.MEDIAID);
		this.format = getElementContent(WXXmlElementName.FORMAT);
		this.recognition = getElementContent(WXXmlElementName.RECOGNITION);
		this.msgId = getElementContent(WXXmlElementName.MSG_ID);
	}

	/**
	 * @return the mediaId
	 */
	public String getMediaId() {
		return mediaId;
	}

	/**
	 * @param mediaId
	 *            the mediaId to set
	 */
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * @param format
	 *            the format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	/**
	 * @return the recognition
	 */
	public String getRecognition() {
		return recognition;
	}

	/**
	 * @param recognition
	 *            the recognition to set
	 */
	public void setRecognition(String recognition) {
		this.recognition = recognition;
	}

	/**
	 * @return the msgId
	 */
	public String getMsgId() {
		return msgId;
	}

	/**
	 * @param msgId
	 *            the msgId to set
	 */
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	@Override
	public JsonObject toJson() {
		JsonObject con = new JsonObject();
		con.put(WXJsonName.MEDIA_ID, mediaId);
		JsonObject msg = head.toJson();
		msg.put(WXJsonName.VOICE, con);
		return msg;
	}
}
