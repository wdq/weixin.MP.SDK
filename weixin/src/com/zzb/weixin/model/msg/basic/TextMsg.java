package com.zzb.weixin.model.msg.basic;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.zzb.weixin.constant.Constant.WXMsgType;
import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.json.JsonObject;

/**
 * 文本消息
 * 
 * @author zzb
 */
public class TextMsg extends Msg {

	// 文本消息内容
	private String content;
	// 消息id，64位整型
	private String msgId;

	/**
	 * 默认构造 初始化head对象，主要由开发者调用
	 * */
	public TextMsg() {
		this(new HeadMsg());
	}

	/**
	 * 此构造由程序内部接收微信服务器消息调用
	 * */
	public TextMsg(HeadMsg head) {
		this.head = head;
		this.head.setMsgType(WXMsgType.text);// 设置消息类型
	}
	
	public TextMsg(Document document){
		this.head = new HeadMsg(document);
		setDocument(document);
		read();
	}

	@Override
	public Element write() {
		Element xml = head.write();
		Element contentElement = document
				.createElement(WXXmlElementName.CONTENT);
		contentElement.setTextContent(this.content);
		xml.appendChild(contentElement);
		return xml;
	}

	@Override
	public void read() {
		this.content = getElementContent(WXXmlElementName.CONTENT);
		this.msgId = getElementContent(WXXmlElementName.MSG_ID);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	@Override
	public JsonObject toJson() {
		JsonObject con = new JsonObject();
		con.put(WXJsonName.CONTENT, content);
		JsonObject msg = head.toJson();
		msg.put(WXJsonName.TEXT, con);
		return msg;
	}
}
