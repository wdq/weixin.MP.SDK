package com.zzb.weixin.model;

import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;

public class OauthToken {
	private String accessToken;
	private String expiresIn;
	private String refreshToken;
	private String openid;
	private String scope;
	public OauthToken(JsonObject jo) {
		accessToken = jo.getAsString(WXJsonName.ACCESS_TOKEN);
		expiresIn = jo.getAsString(WXJsonName.EXPIRES_IN);
		refreshToken = jo.getAsString(WXJsonName.REFRESH_TOKEN);
		openid = jo.getAsString(WXJsonName.OPEN_ID);
		scope = jo.getAsString(WXJsonName.SCOPE);
		accessToken = jo.getAsString(WXJsonName.ACCESS_TOKEN);
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getExpiresIn() {
		return expiresIn;
	}
	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}
	public String getRefreshToken() {
		return refreshToken;
	}
	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
}
