package com.zzb.weixin.model.QRcode;

import com.zzb.weixin.constant.Constant.WXQRActionName;
import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;

/**
 * @author Administrator
 * 
 */
public class QRCodeInfo {
	/**
	 * 该二维码有效时间，以秒为单位。 最大不超过1800。
	 */
	private String expireSeconds;
	/**
	 * 二维码类型，QR_SCENE为临时,QR_LIMIT_SCENE为永久
	 */
	private WXQRActionName actionName;
	/**
	 * 二维码详细信息
	 */
	private String actionInfo;
	/**
	 * 场景值ID，临时二维码时为32位非0整型，永久二维码时最大值为100000（目前参数只支持1--100000）
	 */
	private String sceneId;

	public String getExpireSeconds() {
		return expireSeconds;
	}

	public void setExpireSeconds(String expireSeconds) {
		this.expireSeconds = expireSeconds;
	}

	public WXQRActionName getActionName() {
		return actionName;
	}

	public void setActionName(WXQRActionName actionName) {
		this.actionName = actionName;
	}

	public String getActionInfo() {
		return actionInfo;
	}

	public void setActionInfo(String actionInfo) {
		this.actionInfo = actionInfo;
	}

	public String getSceneId() {
		return sceneId;
	}

	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}

	public JsonObject toJson() {
		JsonObject jsonObject = new JsonObject();
		if (WXJsonName.QR_SCENE.equals(actionName)) {
			jsonObject.put(WXJsonName.ACTION_INFO, actionInfo);
			jsonObject.put(WXJsonName.ACTION_NAME, actionName.toString());
			jsonObject.put(WXJsonName.SCENE_ID, sceneId);
			jsonObject.put(WXJsonName.EXPIRE_SECONDS, expireSeconds);
		} else if (WXJsonName.QR_SCENE.equals(actionName)) {
			jsonObject.put(WXJsonName.ACTION_INFO, actionInfo);
			jsonObject.put(WXJsonName.ACTION_NAME, actionName.toString());
			jsonObject.put(WXJsonName.SCENE_ID, sceneId);
		} else
			return null;
		return jsonObject;
	}

}
