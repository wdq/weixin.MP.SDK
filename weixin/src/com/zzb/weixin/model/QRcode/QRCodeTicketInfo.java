package com.zzb.weixin.model.QRcode;

import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;

public class QRCodeTicketInfo {
	private String ticket;//获取的二维码ticket，凭借此ticket可以在有效时间内换取二维码。
	private String expireSeconds;//二维码的有效时间，以秒为单位。最大不超过1800。
	private String url;//二维码图片解析后的地址，开发者可根据该地址自行生成需要的二维码图片
	public QRCodeTicketInfo(JsonObject jo) {
		ticket = jo.getAsString(WXJsonName.TICKET);
		expireSeconds = jo.getAsString(WXJsonName.EXPIRE_SECONDS);
		url = jo.getAsString(WXJsonName.URL);
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public String getExpireSeconds() {
		return expireSeconds;
	}
	public void setExpireSeconds(String expireSeconds) {
		this.expireSeconds = expireSeconds;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
