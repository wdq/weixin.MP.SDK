package com.zzb.weixin.model.event;

import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.model.msg.basic.HeadMsg;

public class LocationSelectMsg extends EventMsg {

	private String eventKey;// 地理位置纬度
	private String location_X;// 地理位置经度
	private String location_Y;// 地理位置精度
	private String scale;// 地理位置精度
	private String label;// 地理位置精度
	private String poiname;// 地理位置精度
	public LocationSelectMsg(HeadMsg head) {
		super(head);
	}@Override
	public void read() {
		super.read();
		this.eventKey = getElementContent(WXXmlElementName.EVENT_KEY);
		this.location_X = getElementContent(WXXmlElementName.LOCATION_X);
		this.location_Y = getElementContent(WXXmlElementName.LOCATION_Y);
		this.scale = getElementContent(WXXmlElementName.SCALE);
		this.label = getElementContent(WXXmlElementName.LABEL);
		this.poiname = getElementContent(WXXmlElementName.POI_NAME);
	}
	public String getEventKey() {
		return eventKey;
	}
	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
	public String getLocation_X() {
		return location_X;
	}
	public void setLocation_X(String location_X) {
		this.location_X = location_X;
	}
	public String getLocation_Y() {
		return location_Y;
	}
	public void setLocation_Y(String location_Y) {
		this.location_Y = location_Y;
	}
	public String getScale() {
		return scale;
	}
	public void setScale(String scale) {
		this.scale = scale;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getPoiname() {
		return poiname;
	}
	public void setPoiname(String poiname) {
		this.poiname = poiname;
	}
}
