package com.zzb.weixin.model.event;

import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.model.msg.basic.HeadMsg;

/**
 * 群发消息完成事件推送
 * 
 * @author zb.zhang
 * 
 */
public class MsgSendJobFinishMsg extends EventMsg {

	private String msgID;// 群发的消息ID
	private String status;/*
						 * 群发的结构，为“send success”或“send fail”或“err(num)”。但send
						 * success时
						 * ，也有可能因用户拒收公众号的消息、系统错误等原因造成少量用户接收失败。err(num)是审核失败的具体原因
						 * ，可能的情况如下： err(10001), //涉嫌广告 err(20001), //涉嫌政治
						 * err(20004), //涉嫌社会 err(20002), //涉嫌色情 err(20006),
						 * //涉嫌违法犯罪 err(20008), //涉嫌欺诈 err(20013), //涉嫌版权
						 * err(22000), //涉嫌互推(互相宣传) err(21000), //涉嫌其他
						 */
	private long totalCount;//group_id下粉丝数；或者openid_list中的粉丝数
	private long filterCount;//过滤（过滤是指特定地区、性别的过滤、用户设置拒收的过滤，用户接收已超4条的过滤）后，准备发送的粉丝数，原则上，FilterCount = SentCount + ErrorCount
	private long sentCount;//发送成功的粉丝数
	private long errorCount;//发送失败的粉丝数
	public MsgSendJobFinishMsg(HeadMsg head) {
		super(head);
	}
	@Override
	public void read() {
		super.read();
		this.msgID = getElementContent(WXXmlElementName.MSG_ID);
		this.status = getElementContent(WXXmlElementName.STATUS);
		String count = getElementContent(WXXmlElementName.TOTAL_COUNT);
		this.totalCount = count == null ? 0 : Long.valueOf(count);
		count = getElementContent(WXXmlElementName.FILTER_COUNT);
		this.filterCount = count == null ? 0 : Long.valueOf(count);
		count = getElementContent(WXXmlElementName.SENT_COUNT);
		this.sentCount = count == null ? 0 : Long.valueOf(count);
		count = getElementContent(WXXmlElementName.ERROR_COUNT);
		this.errorCount = count == null ? 0 : Long.valueOf(count);
	}
	public String getMsgID() {
		return msgID;
	}
	public void setMsgID(String msgID) {
		this.msgID = msgID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
	public long getFilterCount() {
		return filterCount;
	}
	public void setFilterCount(long filterCount) {
		this.filterCount = filterCount;
	}
	public long getSentCount() {
		return sentCount;
	}
	public void setSentCount(long sentCount) {
		this.sentCount = sentCount;
	}
	public long getErrorCount() {
		return errorCount;
	}
	public void setErrorCount(long errorCount) {
		this.errorCount = errorCount;
	}

}
