package com.zzb.weixin.model.event;

import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.model.msg.basic.HeadMsg;

public class ScanMsg extends EventMsg {
	private String eventKey;//是一个32位无符号整数，即创建二维码时的二维码scene_id
	private String ticket;//二维码的ticket，可用来换取二维码图片
	
	public ScanMsg(HeadMsg head) {
		super(head);
	}

	@Override
	public void read() {
		super.read();
		this.eventKey = getElementContent(WXXmlElementName.EVENT_KEY);
		this.ticket = getElementContent(WXXmlElementName.TICKET);
	}
	public String getEventKey() {
		return eventKey;
	}
	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
}
