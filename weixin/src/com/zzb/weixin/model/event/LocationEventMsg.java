package com.zzb.weixin.model.event;

import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.model.msg.basic.HeadMsg;

public class LocationEventMsg extends EventMsg {

	private String latitude;// 地理位置纬度
	private String longitude;// 地理位置经度
	private String precision;// 地理位置精度
	public LocationEventMsg(HeadMsg head) {
		super(head);
	}@Override
	public void read() {
		super.read();
		this.latitude = getElementContent(WXXmlElementName.LATITUDE);
		this.longitude = getElementContent(WXXmlElementName.LONGITUDE);
		this.precision = getElementContent(WXXmlElementName.PRECISION);
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getPrecision() {
		return precision;
	}
	public void setPrecision(String precision) {
		this.precision = precision;
	}

}
