package com.zzb.weixin.model.event;

import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.model.msg.basic.HeadMsg;

public class SubscribeMsg extends EventMsg {

	private String eventKey;//事件KEY值，qrscene_为前缀，后面为二维码的参数值
	private String ticket;//二维码的ticket，可用来换取二维码图片
	public SubscribeMsg(HeadMsg head) {
		super(head);
	}
	@Override
	public void read() {
		super.read();
		this.eventKey = getElementContent(WXXmlElementName.EVENT_KEY);
		this.ticket = getElementContent(WXXmlElementName.TICKET);
	}
	public String getEventKey() {
		return eventKey;
	}
	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
}
