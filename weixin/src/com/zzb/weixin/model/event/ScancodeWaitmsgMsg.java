package com.zzb.weixin.model.event;

import org.w3c.dom.Node;

import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.model.msg.basic.HeadMsg;

public class ScancodeWaitmsgMsg extends EventMsg {

	private String eventKey;//与自定义菜单接口中KEY值对应
	private String scanType;//扫描类型，一般是qrcode
	private Node scanCodeInfo;//扫描信息
	private String scanResult;//扫描结果，即二维码对应的字符串信息
	public ScancodeWaitmsgMsg(HeadMsg head) {
		super(head);
	}
	@Override
	public void read() {
		super.read();
		this.eventKey = getElementContent(WXXmlElementName.EVENT_KEY);
		this.scanType = getElementContent(WXXmlElementName.SCAN_TYPE);
		this.scanCodeInfo = document.getElementsByTagName(WXXmlElementName.SCAN_CODE_INFO).item(0);
		this.scanResult = getElementContent(WXXmlElementName.SCAN_RESULT);
	}
	public String getEventKey() {
		return eventKey;
	}
	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
	public String getScanType() {
		return scanType;
	}
	public void setScanType(String scanType) {
		this.scanType = scanType;
	}
	public Node getScanCodeInfo() {
		return scanCodeInfo;
	}
	public void setScanCodeInfo(Node scanCodeInfo) {
		this.scanCodeInfo = scanCodeInfo;
	}
	public String getScanResult() {
		return scanResult;
	}
	public void setScanResult(String scanResult) {
		this.scanResult = scanResult;
	}
}
