package com.zzb.weixin.model.event;

import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.model.msg.basic.HeadMsg;

public class TemplatesEndJobFinishMsg extends EventMsg {
	private String msgID;
	private String status;//success-成功；failed:user block- 用户拒绝接受 ；failed: system failed-其他原因失败
	public TemplatesEndJobFinishMsg(HeadMsg head) {
		super(head);
	}
	@Override
	public void read() {
		super.read();
		this.msgID = getElementContent(WXXmlElementName.MSG_ID);
		this.status = getElementContent(WXXmlElementName.STATUS);
	}
	public String getMsgID() {
		return msgID;
	}
	public void setMsgID(String msgID) {
		this.msgID = msgID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
