package com.zzb.weixin.model.event;

import org.w3c.dom.Element;

import com.zzb.util.StringUtil;
import com.zzb.weixin.constant.Constant.WXEvent;
import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.json.JsonObject;
import com.zzb.weixin.model.msg.basic.HeadMsg;
import com.zzb.weixin.model.msg.basic.Msg;

/**
 * 事件消息 注意： 此消息只能是微信服务器推送过来
 * 
 * @author zzb
 * */
public class EventMsg extends Msg {

	// 事件类型subscribe(订阅)、unsubscribe(取消订阅)、CLICK(自定义菜单点击事件)
	private WXEvent event;
	// 事件KEY值，与自定义菜单接口中KEY值对应
	private String eventKey;
	// 二维码的ticket，可用来换取二维码图片
	private String ticket;

	private String latitude;// 地理位置纬度
	private String longitude;// 地理位置经度
	private String precision;// 地理位置精度

	/**
	 * 程序内部调用
	 * */
	public EventMsg(HeadMsg head) {
		this.head = head;
	}

	/**
	 * 因为此消息都是由微信服务器发送给我们的，我们不用发给微信服务器，因此不用实现write
	 * */
	@Override
	public Element write() {
		return null;
	}

	@Override
	public void read() {
		String event = getElementContent(WXXmlElementName.EVENT);
		if(!StringUtil.isEmpty(event)){
			this.event = WXEvent.valueOf(event.toUpperCase());
		}
	}

	public WXEvent getEvent() {
		return event;
	}

	public void setEvent(WXEvent event) {
		this.event = event;
	}

	public String getEventKey() {
		return eventKey;
	}

	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}

	/**
	 * @return the ticket
	 */
	public String getTicket() {
		return ticket;
	}

	/**
	 * @param ticket
	 *            the ticket to set
	 */
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the precision
	 */
	public String getPrecision() {
		return precision;
	}

	/**
	 * @param precision
	 *            the precision to set
	 */
	public void setPrecision(String precision) {
		this.precision = precision;
	}
	//事件不需要转为客服消息结构
	@Override
	public JsonObject toJson() {
		return null;
	}

}
