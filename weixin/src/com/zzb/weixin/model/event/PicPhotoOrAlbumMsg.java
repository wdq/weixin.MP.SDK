package com.zzb.weixin.model.event;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.NodeList;

import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.model.msg.basic.HeadMsg;

public class PicPhotoOrAlbumMsg extends EventMsg {

	private String eventKey;//与自定义菜单接口中KEY值对应
	private String sendPicsInfo;//发送的图片信息
	private String count;//扫描信息
	private List<String> picMd5SumList;//图片的MD5值，开发者若需要，可用于验证接收到图片
	public PicPhotoOrAlbumMsg(HeadMsg head) {
		super(head);
	}
	@Override
	public void read() {
		super.read();
		this.eventKey = getElementContent(WXXmlElementName.EVENT_KEY);
		this.sendPicsInfo = getElementContent(WXXmlElementName.SEND_PICS_INFO);
		this.count = getElementContent(WXXmlElementName.COUNT);
		NodeList nodeList = document.getElementsByTagName(WXXmlElementName.PIC_MD5_SUM);
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < nodeList.getLength(); i++) {
			list.add(nodeList.item(i).getTextContent());
		}
		this.setPicMd5SumList(list);
	}
	public String getEventKey() {
		return eventKey;
	}
	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
	public String getSendPicsInfo() {
		return sendPicsInfo;
	}
	public void setSendPicsInfo(String sendPicsInfo) {
		this.sendPicsInfo = sendPicsInfo;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public List<String> getPicMd5SumList() {
		return picMd5SumList;
	}
	public void setPicMd5SumList(List<String> picMd5SumList) {
		this.picMd5SumList = picMd5SumList;
	}
}
