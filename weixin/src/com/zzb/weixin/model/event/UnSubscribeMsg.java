package com.zzb.weixin.model.event;

import com.zzb.weixin.model.msg.basic.HeadMsg;

public class UnSubscribeMsg extends EventMsg {

	public UnSubscribeMsg(HeadMsg head) {
		super(head);
	}

}
