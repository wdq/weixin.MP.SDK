package com.zzb.weixin.model.event;

import com.zzb.weixin.constant.WXXmlElementName;
import com.zzb.weixin.model.msg.basic.HeadMsg;

public class ClickMsg extends EventMsg {

	private String eventKey;//与自定义菜单接口中KEY值对应
	public ClickMsg(HeadMsg head) {
		super(head);
	}
	@Override
	public void read() {
		super.read();
		this.eventKey = getElementContent(WXXmlElementName.EVENT_KEY);
	}
	public String getEventKey() {
		return eventKey;
	}
	public void setEventKey(String eventKey) {
		this.eventKey = eventKey;
	}
}
