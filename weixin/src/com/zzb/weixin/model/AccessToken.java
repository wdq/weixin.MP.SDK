package com.zzb.weixin.model;

import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;

public class AccessToken {
	private String accessToken;//获取到的凭证
	private String expiresIn;//凭证有效时间，单位：秒
	public AccessToken() {
	}
	public AccessToken(JsonObject jo) {
		this.setAccessToken(jo.getAsString(WXJsonName.ACCESS_TOKEN));
		this.setExpiresIn(jo.getAsString(WXJsonName.EXPIRES_IN));
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getExpiresIn() {
		return expiresIn;
	}
	public void setExpiresIn(String expiresIn) {
		this.expiresIn = expiresIn;
	}
}
