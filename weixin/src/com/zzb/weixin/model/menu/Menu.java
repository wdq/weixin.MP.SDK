package com.zzb.weixin.model.menu;

import java.util.ArrayList;
import java.util.List;

import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonArray;
import com.zzb.weixin.json.JsonObject;
/**
 * 微信菜单的最外层结构，每一个内层菜单是一个menuItem对象,最多有3个子节点
 * @author zb.zhang
 *
 */
public class Menu {
	private List<MenuItem> button;
	public Menu() {
		button = new ArrayList<MenuItem>(3);
	}

	public Menu(JsonObject jo) {
		jo = jo.getAsJsonObject(WXJsonName.MENU);
		if (jo != null) {
			List<JsonObject> list = jo.getAsJsonArray(WXJsonName.BUTTON)
					.getAsJsonObjectList();
			for (JsonObject jsonObject : list) {
				button.add(new MenuItem(jsonObject));
			}
		}
	}

	public List<MenuItem> getButton() {
		return button;
	}
	/**
	 * 添加一级菜单,最多3个
	 * @param button
	 */
	public void setButton(List<MenuItem> button) {
		this.button = button;
	}
	
	public void addButton(MenuItem menuItem){
		if(this.button == null) button = new ArrayList<>(3);
		if(this.button.size() >= 3)return ;
		button.add(menuItem);
	}

	public JsonObject toJson() {
		JsonObject jo = new JsonObject();
		JsonArray ja = new JsonArray();
		for (MenuItem menu : button) {
			ja.add(menu.toJsonObject());
		}
		jo.put(WXJsonName.BUTTON, ja);
		return jo;
	}
}
