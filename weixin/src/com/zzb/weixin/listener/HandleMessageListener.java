package com.zzb.weixin.listener;

import com.zzb.weixin.model.event.EventMsg;
import com.zzb.weixin.model.msg.basic.ImageMsg;
import com.zzb.weixin.model.msg.basic.LinkMsg;
import com.zzb.weixin.model.msg.basic.LocationMsg;
import com.zzb.weixin.model.msg.basic.Msg;
import com.zzb.weixin.model.msg.basic.TextMsg;
import com.zzb.weixin.model.msg.basic.VideoMsg;
import com.zzb.weixin.model.msg.basic.VoiceMsg;

/**
 * 主要用于接收微信服务器消息的接口
 * 
 * @author marker
 * */
public interface HandleMessageListener {

	/**
	 * 收到文本消息
	 * 
	 * @param msg
	 */
	public boolean onMsg(TextMsg msg);

	/**
	 * 收到图片消息
	 * 
	 * @param msg
	 */
	public boolean onMsg(ImageMsg msg);

	/**
	 * 收到事件推送消息
	 * 
	 * @param msg
	 */
	public boolean onMsg(EventMsg msg);

	/**
	 * 收到链接消息
	 * 
	 * @param msg
	 */
	public boolean onMsg(LinkMsg msg);

	/**
	 * 收到地理位置消息
	 * 
	 * @param msg
	 */
	public boolean onMsg(LocationMsg msg);

	/**
	 * 语音识别消息
	 * 
	 * @param msg
	 */
	public boolean onMsg(VoiceMsg msg);

	/**
	 * 错误消息
	 * 
	 * @param msg
	 */
	public boolean onMsg(int errorCode);

	/**
	 * 视频消息
	 * 
	 * @param msg
	 */
	public boolean onMsg(VideoMsg msg);
	
	public boolean onMsg(Msg msg);

}
