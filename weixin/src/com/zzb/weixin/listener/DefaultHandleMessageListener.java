package com.zzb.weixin.listener;

import com.zzb.weixin.model.event.EventMsg;
import com.zzb.weixin.model.msg.basic.ImageMsg;
import com.zzb.weixin.model.msg.basic.LinkMsg;
import com.zzb.weixin.model.msg.basic.LocationMsg;
import com.zzb.weixin.model.msg.basic.Msg;
import com.zzb.weixin.model.msg.basic.TextMsg;
import com.zzb.weixin.model.msg.basic.VideoMsg;
import com.zzb.weixin.model.msg.basic.VoiceMsg;

public class DefaultHandleMessageListener implements HandleMessageListener{

	@Override
	public boolean onMsg(TextMsg msg) {
		return false;
	}

	@Override
	public boolean onMsg(ImageMsg msg) {
		return false;
	}

	@Override
	public boolean onMsg(EventMsg msg) {
		return false;
	}

	@Override
	public boolean onMsg(LinkMsg msg) {
		return false;
	}

	@Override
	public boolean onMsg(LocationMsg msg) {
		return false;
	}

	@Override
	public boolean onMsg(VoiceMsg msg) {
		return false;
	}

	@Override
	public boolean onMsg(int errorCode) {
		return false;
	}

	@Override
	public boolean onMsg(VideoMsg msg) {
		return false;
	}

	@Override
	public boolean onMsg(Msg msg){
		if(msg instanceof ImageMsg){
			return onMsg((ImageMsg)msg);
		}else if(msg instanceof LinkMsg){
			return onMsg((LinkMsg)msg);
		}else if(msg instanceof LocationMsg){
			return onMsg((LocationMsg)msg);
		}else if(msg instanceof TextMsg){
			return onMsg((TextMsg)msg);
		}else if(msg instanceof VideoMsg){
			return onMsg((VideoMsg)msg);
		}else if(msg instanceof VoiceMsg){
			return onMsg((VoiceMsg)msg);
		}else if(msg instanceof EventMsg){
			return onMsg((EventMsg)msg);
		}
		return false;
	}

}
