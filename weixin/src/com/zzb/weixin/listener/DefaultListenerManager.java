package com.zzb.weixin.listener;

import java.util.ArrayList;
import java.util.List;

import com.zzb.weixin.model.msg.basic.Msg;

/**
 * 默认的ListenerManager实现
 * 使用默认ListenerManager实现类需要通过添加HandleMessageListener监听器来对微信消息进行监听
 * 可以添加多个监听器来处理不同的消息内容。
 * 
 * 注意：此类并非线程安全，建议每次使用newInstance调用
 * 
 * @author zzb
 * */
public class DefaultListenerManager extends ListenerManager {

	/** 监听器集合 */
	private List<HandleMessageListener> listeners = new ArrayList<HandleMessageListener>(
			3);

	/**
	 * 私有构造方法
	 */
	private DefaultListenerManager() {
	}

	private static DefaultListenerManager defaultSession = new DefaultListenerManager();

	/**
	 * 创建一个ListenerManager实例
	 * */
	public static DefaultListenerManager newInstance() {
		return defaultSession;
	}

	/**
	 * 添加监听器
	 * 
	 * @param handleMassge
	 */
	public void addOnHandleMessageListener(HandleMessageListener handleMassge) {
		listeners.add(handleMassge);
	}

	/**
	 * 移除监听器
	 * */
	public void removeOnHandleMessageListener(HandleMessageListener handleMassge) {
		listeners.remove(handleMassge);
	}

	@Override
	public void onMsg(Msg msg) {
		boolean isResult = true;
		for (HandleMessageListener currentListener : listeners) {
			if(currentListener.onMsg(msg)){
				isResult = false;
				break;
			}
		}
		if(isResult)callback();//如果没有定义响应,自动返回空字符传
	}

	@Override
	public void onMsg(int errorCode) {
		boolean isResult = true;
		for (HandleMessageListener currentListener : listeners) {
			if(currentListener.onMsg(errorCode)){
				isResult = false;
				break;
			}
		}
		if(isResult)callback();//如果没有定义响应,自动返回空字符传
	}
}
