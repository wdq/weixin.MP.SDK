package com.zzb.weixin;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.zzb.util.SecurityUtil;
import com.zzb.weixin.constant.Constant.WXLang;
import com.zzb.weixin.constant.Constant.WXMediaType;
import com.zzb.weixin.constant.Constant.WXOauthScope;
import com.zzb.weixin.exception.WeiXinException;
import com.zzb.weixin.json.JsonObject;
import com.zzb.weixin.model.AccessToken;
import com.zzb.weixin.model.OauthToken;
import com.zzb.weixin.model.UploadMsg;
import com.zzb.weixin.model.QRcode.QRCodeInfo;
import com.zzb.weixin.model.QRcode.QRCodeTicketInfo;
import com.zzb.weixin.model.menu.Menu;
import com.zzb.weixin.model.msg.basic.Msg;
import com.zzb.weixin.model.user.WeiXinUser;
import com.zzb.weixin.model.user.WeiXinUserIdList;

public class WeiXinUtil {

	/**
	 * 
	 * @param token
	 * @param timestamp
	 * @param nonce
	 * @param signature
	 * @return
	 */
	public static boolean check(String token, String timestamp, String nonce,
			String signature) {
		List<String> list = new ArrayList<String>(3);
		list.add(token);
		list.add(timestamp);
		list.add(nonce);
		Collections.sort(list);// 排序
		String tmpStr = new SecurityUtil().encode(list.get(0) + list.get(1)
				+ list.get(2), SecurityUtil.SHA_1);// SHA-1加密
		if (tmpStr.equals(signature)) {
			return true;
		}
		return false;
	}

	/**
	 * 获取accessToken对象 appid appsecret 将从配置文件中读取
	 * 
	 * access_token是公众号的全局唯一票据，公众号调用各接口时都需使用access_token。
	 * 正常情况下access_token有效期为7200秒，重复获取将导致上次获取的access_token失效。
	 * 由于获取access_token的api调用次数非常有限
	 * ，建议开发者全局存储与更新access_token，频繁刷新access_token会导致api调用受限，影响自身业务。
	 * 请开发者注意，由于技术升级
	 * ，公众平台的开发接口的access_token长度将增长，其存储至少要保留512个字符空间。此修改将在1个月后生效，请开发者尽快修改兼容。
	 * 公众号可以使用AppID和AppSecret调用本接口来获取access_token
	 * 。AppID和AppSecret可在开发模式中获得（需要已经成为开发者，且帐号没有异常状态）。注意调用所有微信接口时均需使用https协议。
	 * 
	 * @return
	 * @throws WeiXinException
	 *             微信错误信息对象
	 */
	public static AccessToken getAccessToken() throws WeiXinException {
		return WeiXinService.getAccessToken();
	}

	/**
	 * 获取accessToken对象
	 * 
	 * @param appId
	 * @param appSecret
	 * @return
	 * @throws WeiXinException
	 */
	public static AccessToken getAccessToken(String appId, String appSecret)
			throws WeiXinException {
		return WeiXinService.getAccessToken(appId, appSecret);
	}

	/**
	 * 如果公众号基于安全等考虑，需要获知微信服务器的IP地址列表，以便进行相关限制，可以通过该接口获得微信服务器IP地址列表。
	 * 
	 * @param accessToken
	 *            公众号的access_token
	 * @return
	 * @throws WeiXinException
	 */
	public static List<String> getIPList(String accessToken)
			throws WeiXinException {
		return WeiXinService.getIPList(accessToken);
	}

	/**
	 * 公众号可调用本接口来获取多媒体文件。请注意，视频文件不支持下载，调用该接口需http协议。
	 * 
	 * @param filepath
	 *            文件路径，不包括文件名
	 * @param mediaId
	 * @param accessToken
	 * @return
	 * @throws WeiXinException
	 */
	public static File downloadMedia(String filepath, String mediaId,
			String accessToken) throws WeiXinException {
		return WeiXinService.downloadMedia(filepath, mediaId, accessToken);
	}

	/**
	 * 获取微信用户信息
	 * 
	 * @param accessToken
	 * @param openid
	 * @return
	 * @throws WeiXinException
	 */

	public static WeiXinUser getWeiXinUser(String accessToken,
			String openid, WXLang lang) throws WeiXinException {
		JsonObject jo = WeiXinService.getWeiXinUserAsJson(accessToken, openid,
				lang);
		return new WeiXinUser(jo);
	}

	/**
	 * 公众号可调用本接口来上传图片、语音、视频等文件到微信服务器，上传后服务器会返回对应的media_id，
	 * 公众号此后可根据该media_id来获取多媒体。请注意，media_id是可复用的，调用该接口需http协议。
	 * 
	 * @param file
	 *            要上传的媒体文件
	 * @param type
	 *            文件类型
	 * @param accessToken
	 * @return
	 * @throws WeiXinException
	 */
	public static UploadMsg uploadMedia(File file, WXMediaType type,
			String accessToken) throws WeiXinException {
		return WeiXinService.uploadMedia(file, type, accessToken);
	}

	public static void sendMsg(String accessToken, Msg msg)
			throws WeiXinException {
		WeiXinService.sendCustomMsg(accessToken, msg);
	}

	public static WeiXinUserIdList getWeiXinUserIds(String accessToken,
			String nextOpenId) throws WeiXinException {
		return WeiXinService.getUserIdList(accessToken, nextOpenId);
	}

	public static boolean deleteMenu(String accessToken) throws WeiXinException {
		return WeiXinService.deleteMenu(accessToken);
	}

	public static boolean createMenu(String accessToken, Menu menu)
			throws WeiXinException {
		return WeiXinService.createMenu(accessToken, menu);
	}

	public static Menu getMenu(String url, String accessToken)
			throws WeiXinException {
		return WeiXinService.getMenu(accessToken);
	}

	public static QRCodeTicketInfo createTicket(String accessToken,
			QRCodeInfo codeInfo) throws WeiXinException {
		return WeiXinService.createQRcode(accessToken, codeInfo);
	}

	public static String getCodeUrl(String appid, String returnUrl,
			WXOauthScope scope, String state) {
		return WeiXinService.getCodeUrl(appid, returnUrl, scope, state);
	}

	public static String getCodeUrl(String returnUrl, WXOauthScope scope,
			String state) {
		return WeiXinService.getCodeUrl(returnUrl, scope, state);
	}

	public static OauthToken getOauthToken(String appid, String secret,
			String code) throws WeiXinException {
		return WeiXinService.getOauthToken(appid, secret, code);
	}

	public static OauthToken getOauthToken(String code) throws WeiXinException {
		return WeiXinService.getOauthToken(code);
	}

	public static OauthToken refreshOauthToken(String appid, String refreshToken)
			throws WeiXinException {
		return WeiXinService.refushOauthToken(appid, refreshToken);
	}

	public static OauthToken refreshOauthToken(String refreshToken)
			throws WeiXinException {
		return WeiXinService.refushOauthToken(refreshToken);
	}
}
