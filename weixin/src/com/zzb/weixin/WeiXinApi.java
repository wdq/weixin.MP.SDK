package com.zzb.weixin;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.zzb.weixin.constant.Constant.WXLang;
import com.zzb.weixin.constant.Constant.WXMediaType;
import com.zzb.weixin.constant.Constant.WXOauthScope;
import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.constant.WXJsonValue;
import com.zzb.weixin.exception.WeiXinException;
import com.zzb.weixin.json.JSON;
import com.zzb.weixin.json.JsonObject;

public class WeiXinApi {
	private final static String end = "\r\n";
	private final static String twoHyphens = "--"; // 用于拼接

	public static JsonObject getAccessToken(String appId, String appSecret)
			throws WeiXinException {
		return doGet(WeiXinUrlService.getGetAccessTokenUrl(appId, appSecret));
	}

	/**
	 * 如果公众号基于安全等考虑，需要获知微信服务器的IP地址列表，以便进行相关限制，可以通过该接口获得微信服务器IP地址列表。
	 * 
	 * @param url
	 * @param accessToken
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject getIPListAsJson(String accessToken)
			throws WeiXinException {
		return doGet(WeiXinUrlService.getGetIPListURL(accessToken));
	}

	/**
	 * 公众号可调用本接口来上传图片、语音、视频等文件到微信服务器，上传后服务器会返回对应的media_id，
	 * 公众号此后可根据该media_id来获取多媒体。请注意，media_id是可复用的，调用该接口需http协议。
	 * 
	 * @param file
	 * @param type
	 *            媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
	 * @param accessToken
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject uploadMedia(File file, WXMediaType type,
			String accessToken) throws WeiXinException {
		JsonObject jo = null;
		try {
			String url = WeiXinUrlService.getUploadMediaUrl(accessToken, type);
			URL u = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) u.openConnection();
			conn.setDoOutput(true);

			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("Content-Type", "multipart/form-data;");
			// 获取输出流对象，准备上传文件
			OutputStream dos = conn.getOutputStream();
			dos.write((twoHyphens + end).getBytes());
			dos.write(("Content-Disposition: form-data; name=\""
					+ file.getName() + "\";filename=\"" + file.getName()
					+ ";filelength=\"" + file.length()
					+ "content-type=\"application/octet-stream\"" + end)
					.getBytes());
			dos.write(end.getBytes());
			// 对文件进行传输
			FileInputStream fis = new FileInputStream(file);
			byte[] buffer = new byte[1024]; // 1k
			int count = 0;
			while ((count = fis.read(buffer)) != -1) {
				dos.write(buffer, 0, count);
			}
			fis.close(); // 关闭文件流

			dos.write(end.getBytes());
			dos.write((twoHyphens + end).getBytes());
			dos.flush();
			dos.close();

			jo = JSON.parse(getContext(conn.getInputStream()));
			doExeption(jo);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jo;
	}

	/**
	 * 公众号可调用本接口来获取多媒体文件。请注意，视频文件不支持下载，调用该接口需http协议。
	 * 
	 * @param filepath
	 * @param mediaId
	 * @param accessToken
	 * @throws WeiXinException
	 */
	public static File downloadMedia(String filepath, String mediaId,
			String accessToken) throws WeiXinException {
		String url = WeiXinUrlService.getDownloadMediaUrl(accessToken, mediaId);
		File file = null;
		try {
			HttpURLConnection conn = (HttpURLConnection) new URL(url)
					.openConnection();
			conn.getContentType();
			String fileName = conn.getHeaderField("Content-disposition");
			Pattern p = Pattern.compile("\"[^\"]*\"");
			Matcher m = p.matcher(fileName);
			if (m.find()) {
				fileName = m.group();
				fileName = fileName.substring(1, fileName.length() - 1);
				downLoadFile(url, filepath, fileName);
			} else {
				JsonObject jo = JSON
						.parse(getContext(new URL(url).openStream()));
				doExeption(jo);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}

	/**
	 * 将指定url资源下载到指定目录的文件中
	 * 
	 * @param url
	 * @param filepath
	 * @param fileName
	 * @return
	 */
	public static File downLoadFile(String url, String filepath, String fileName) {
		InputStream is = null;
		OutputStream os = null;
		File file = null;
		try {
			is = new URL(url).openStream();
			file = new File(filepath + fileName);
			int i = 0;
			while (file.exists()) {// 如果文件存在，循环修改文件名
				file = new File(filepath = fileName.replace(".", "(" + i++
						+ ")."));
			}
			os = new FileOutputStream(file);
			byte[] b = new byte[1024];
			int stepLength = 0;
			while ((stepLength = is.read(b)) != -1) {
				os.write(b, 0, stepLength);
			}
			os.flush();
			os.close();
			is.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (os != null) {
					os.close();
				}
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return file;
	}

	/**
	 * 发送客服消息
	 * 
	 * @param accessToken
	 * @param msg
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject sendCustomMsg(String accessToken, JsonObject msg)
			throws WeiXinException {
		return doPost(WeiXinUrlService.getSendCustomMsgUrl(accessToken), msg);
	}

	/**
	 * 获取json格式的微信用户数据
	 * 
	 * @param url
	 * @param accessToken
	 * @param openid
	 * @param lang
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject getWeiXinUserAsJson(String accessToken,
			String openid, WXLang lang) throws WeiXinException {
		return doGet(WeiXinUrlService.getGetUserInfoUrl(accessToken, openid,
				lang));
	}

	/**
	 * 上传图文消息素材供群发消息使用
	 * 
	 * @param accessToken
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject uploadNews(String accessToken, JsonObject articles)
			throws WeiXinException {
		return doPost(WeiXinUrlService.getUploadNewsUrl(accessToken), articles);
	}

	/**
	 * 上传视频素材供群发使用
	 * 
	 * @param accessToken
	 * @param video
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject uploadVidoe(String accessToken, JsonObject video)
			throws WeiXinException {
		return doPost(WeiXinUrlService.getUploadVideoUrl(accessToken), video);
	}

	/**
	 * 根据分组进行群发
	 * 
	 * @param accessToken
	 * @param message
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject sendAll(String accessToken, JsonObject message)
			throws WeiXinException {
		return doPost(WeiXinUrlService.getSendAllUrl(accessToken), message);
	}

	/**
	 * 根据OpenID列表进行群发
	 * 
	 * @param accessToken
	 * @param message
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject send(String accessToken, JsonObject message)
			throws WeiXinException {
		return doPost(WeiXinUrlService.getSendUrl(accessToken), message);
	}

	/**
	 * 发送模板数据
	 * 
	 * @param accessToken
	 * @param message
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject sendTemplateMsg(String accessToken,
			JsonObject message) throws WeiXinException {
		return doPost(WeiXinUrlService.getSendTemlateMsgUrl(accessToken),
				message);
	}

	/**
	 * 请注意，只有已经发送成功的消息才能删除删除消息只是将消息的图文详情页失效，已经收到的用户，还是能在其本地看到消息卡片。
	 * 另外，删除群发消息只能删除图文消息和视频消息，其他类型的消息一经发送，无法删除。
	 * 
	 * @param accessToken
	 * @param message
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject delete(String accessToken, JsonObject message)
			throws WeiXinException {
		return doPost(WeiXinUrlService.getDeleteUrl(accessToken), message);
	}

	/**
	 * 创建分组
	 * 
	 * @param accessToken
	 * @param message
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject createGroup(String accessToken, JsonObject message)
			throws WeiXinException {
		return doPost(WeiXinUrlService.getCreateGroupUrl(accessToken), message);
	}

	/**
	 * 查询分组
	 * 
	 * @param accessToken
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject getGroups(String accessToken)
			throws WeiXinException {
		return doGet(WeiXinUrlService.getCreateGroupUrl(accessToken));
	}

	/**
	 * 创建分组
	 * 
	 * @param accessToken
	 * @param message
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject getGroupByOpenID(String accessToken,
			JsonObject message) throws WeiXinException {
		return doPost(WeiXinUrlService.getGetIdGroupUrl(accessToken), message);
	}

	/**
	 * 修改分组
	 * 
	 * @param accessToken
	 * @param message
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject updateGroup(String accessToken, JsonObject message)
			throws WeiXinException {
		return doPost(WeiXinUrlService.getUpdateGroupUrl(accessToken), message);
	}

	/**
	 * 修改用户分组
	 * 
	 * @param accessToken
	 * @param message
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject updateMemberGroup(String accessToken,
			JsonObject message) throws WeiXinException {
		return doPost(WeiXinUrlService.getUpdateMemberGroupUrl(accessToken),
				message);
	}

	/**
	 * 修改用户备注
	 * 
	 * @param accessToken
	 * @param message
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject updateRemark(String accessToken, JsonObject message)
			throws WeiXinException {
		return doPost(WeiXinUrlService.getUpdateRemarkUrl(accessToken), message);
	}

	/**
	 * 获取用户信息
	 * 
	 * @param accessToken
	 * @param openid
	 * @param lang
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject getUserInfo(String accessToken, String openid,
			WXLang lang) throws WeiXinException {
		return doGet(WeiXinUrlService.getGetUserInfoUrl(accessToken, openid,
				lang));
	}

	/**
	 * 获取关注者openid列表
	 * 
	 * @param accessToken
	 * @param nextOpenid
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject getUserOpenIDs(String accessToken,
			String nextOpenid) throws WeiXinException {
		return doGet(WeiXinUrlService.getGetUserOpenidsUrl(accessToken,
				nextOpenid));
	}
	/**
	 * 获取Oauth认证跳转页面Url
	 * @param appid
	 * @param returnUrl
	 * @param scope
	 * @param state
	 * @return
	 */
	public static String getGetCodeUrl(String appid, String returnUrl,
			WXOauthScope scope, String state) {
		return WeiXinUrlService.getGetCodeUrl(appid, returnUrl, scope, state);
	}

	/**
	 * 
	 * @param appid
	 * @param secret
	 * @param code
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject getOauthAccessToken(String appid, String secret,
			String code) throws WeiXinException {
		return doGet(WeiXinUrlService.getGetOauthAccessTokenUrl(appid, secret,
				code));
	}

	/**
	 * 刷新oauth认证的accessToken
	 * 
	 * @param appid
	 * @param refreshToken
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject refreshOauthAccessToken(String appid,
			String refreshToken) throws WeiXinException {
		return doGet(WeiXinUrlService.getRefreshOauthAccessTokenUrl(
				refreshToken, appid));
	}

	/**
	 * 获取oauth用户信息
	 * 
	 * @param accessToken
	 * @param openid
	 * @param lang
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject getOauthUserInfo(String accessToken,
			String openid, WXLang lang) throws WeiXinException {
		return doGet(WeiXinUrlService.getGetOauthUserInfoUrl(accessToken,
				openid, lang));
	}

	/**
	 * 检查用户oauth accessToken是否有效
	 * 
	 * @param accessToken
	 * @param openid
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject checkOauthAccessToken(String accessToken,
			String openid) throws WeiXinException {
		return doGet(WeiXinUrlService.getCheckOauthAccessTokenUrl(accessToken,
				openid));
	}

	/**
	 * 创建自定义菜单
	 * 
	 * @param accessToken
	 * @param menu
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject createMenu(String accessToken, JsonObject menu)
			throws WeiXinException {
		return doPost(WeiXinUrlService.getCreateMenuUrl(accessToken), menu);
	}

	/**
	 * 查询自定义菜单
	 * 
	 * @param accessToken
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject getMenu(String accessToken) throws WeiXinException {
		return doGet(WeiXinUrlService.getGetMenuUrl(accessToken));
	}

	/**
	 * 删除自定义菜单
	 * 
	 * @param accessToken
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject deleteMenu(String accessToken)
			throws WeiXinException {
		return doGet(WeiXinUrlService.getDeleteMenuUrl(accessToken));
	}

	/**
	 * 创建二维码
	 * 
	 * @param accessToken
	 * @param codeInfo
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject createQrcode(String accessToken,
			JsonObject codeInfo) throws WeiXinException {
		return doPost(WeiXinUrlService.getCreateQRcodeUrl(accessToken),
				codeInfo);
	}

	/**
	 * 获取获取二维码的url
	 * 
	 * @param ticket
	 * @return
	 */
	public static String getGetQRcodeUrl(String ticket) {
		return WeiXinUrlService.getGetQRcodeByTicketUrl(ticket);
	}

	/**
	 * 长链接转短链接
	 * @param accessToken
	 * @param longUrl
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject getShortUrl(String accessToken, String longUrl)
			throws WeiXinException {
		JsonObject message = new JsonObject();
		message.put(WXJsonName.ACTION, WXJsonValue.LONG2SHORT);
		message.put(WXJsonName.LONG_URL, longUrl);
		return doPost(WeiXinUrlService.getShortUrlUrl(accessToken), message);
	}
	/**
	 * 获取客服信息
	 * @param accessToken
	 * @param pageInfo
	 * @return
	 * @throws WeiXinException
	 */
	public static JsonObject getCSRecord(String accessToken,JsonObject pageInfo) throws WeiXinException{
		return doPost(WeiXinUrlService.getCustomServiceRecordUrl(accessToken), pageInfo);
	}

	private static JsonObject doGet(String url) throws WeiXinException {
		JsonObject accessToken = null;
		try {
			if (url.startsWith("https"))
				System.setProperty("https.protocols", "SSLv3,SSLv2Hello");
			URL theUrl = new URL(url);
			accessToken = JSON.parse(getContext(theUrl.openStream()));
			doExeption(accessToken);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return accessToken;
	}

	private static JsonObject doPost(String url, JsonObject message)
			throws WeiXinException {
		JsonObject jo = null;
		try {
			if (url.startsWith("https"))
				System.setProperty("https.protocols", "SSLv3,SSLv2Hello");
			HttpURLConnection connection = (HttpURLConnection) new URL(url)
					.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			connection.setUseCaches(false);
			connection.setInstanceFollowRedirects(true);
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			connection.connect();
			// POST请求
			DataOutputStream out = new DataOutputStream(
					connection.getOutputStream());

			out.writeBytes(message.toString());
			out.flush();
			out.close();
			jo = JSON.parse(getContext(connection.getInputStream()));
			doExeption(jo);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jo;
	}

	/**
	 * 获取输入流的内容
	 * 
	 * @param is
	 * @return
	 * @throws IOException
	 */
	private static String getContext(InputStream is) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader bf = null;
		try {
			bf = new BufferedReader(new InputStreamReader(is));
			String s = bf.readLine();
			while (s != null) {
				sb.append(s);
				s = bf.readLine();
			}
		} catch (IOException e) {
			throw e;
		} finally {
			if (bf != null)
				bf.close();
		}
		return sb.toString();
	}

	private static void doExeption(JsonObject jo) throws WeiXinException {
		String errCode = jo.getAsString(WXJsonName.ERR_CODE);
		if (errCode == null || "".equals(errCode) || "0".equals(errCode))
			return;
		String errMsg = jo.getAsString(WXJsonName.ERR_MSG);
		throw new WeiXinException(errCode, errMsg);
	}
}
