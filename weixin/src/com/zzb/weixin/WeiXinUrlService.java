package com.zzb.weixin;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.zzb.weixin.constant.Constant.WXLang;
import com.zzb.weixin.constant.Constant.WXMediaType;
import com.zzb.weixin.constant.Constant.WXOauthScope;
import com.zzb.weixin.constant.WXUrlName;
import com.zzb.weixin.constant.WXValue;

public class WeiXinUrlService {

	private static String url = "https://api.weixin.qq.com/cgi-bin";
	private static String mediaUrl = "http://file.api.weixin.qq.com/cgi-bin/media";
	private static String codeUrl = "https://open.weixin.qq.com/connect/oauth2/authorize";
	private static String oauthUrl = "https://api.weixin.qq.com/sns";

	/**
	 * 获取accessToken的url
	 * 
	 * @param url
	 * @param grantType
	 * @param appId
	 * @param appSecret
	 * @return
	 */
	public static String getGetAccessTokenUrl(String appId, String appSecret) {
		String returnUrl = url + "/token";
		returnUrl = addGetParam(returnUrl, WXUrlName.GRANT_TYPE,
				WXValue.GRANT_TYPE);
		returnUrl = addGetParam(returnUrl, WXUrlName.APP_ID, appId);
		returnUrl = addGetParam(returnUrl, WXUrlName.APP_SECRET, appSecret);
		return returnUrl;
	}

	/**
	 * 获取ipList 的url
	 * 
	 * @param url
	 * @param accessToken
	 * @return
	 */
	public static String getGetIPListURL(String accessToken) {
		String returnUrl = url + "/getcallbackip";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}

	/**
	 * 获取上传媒体文件
	 * 
	 * @param url
	 * @param accessToken
	 * @param type
	 * @return
	 */
	public static String getUploadMediaUrl(String accessToken, WXMediaType type) {
		String returnUrl = mediaUrl + "/media/upload";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		returnUrl = addGetParam(url, WXUrlName.TYPE, type.toString());
		return returnUrl;
	}

	/**
	 * 获取下载媒体文件url
	 * 
	 * @param url
	 * @param accessToken
	 * @param mediaId
	 * @return
	 */
	public static String getDownloadMediaUrl(String accessToken, String mediaId) {
		String returnUrl = mediaUrl + "/media/get";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		url = addGetParam(url, WXUrlName.MEDIA_ID, mediaId);
		return returnUrl;
	}
	/**
	 * 获取发送客服消息url
	 * @param accessToken
	 * @return
	 */
	public static String getSendCustomMsgUrl(String accessToken){
		String returnUrl = url + "/message/custom/send";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}

	/**
	 * 获取上传图文消息素材url
	 * 
	 * @param url
	 * @param accessToken
	 * @return
	 */
	public static String getUploadNewsUrl(String accessToken) {
		String returnUrl = url + "/media/uploadnews";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}

	/**
	 * 获取视频上传url
	 * 
	 * @param accessToken
	 * @return
	 */
	public static String getUploadVideoUrl(String accessToken) {
		String returnUrl = url + "/media/uploadvideo";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}

	/**
	 * 获取按分组群发消息url
	 * 
	 * @param accessToken
	 * @return
	 */
	public static String getSendAllUrl(String accessToken) {
		String returnUrl = url + "/message/mass/sendall";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}

	/**
	 * 获取按openid列表群发url
	 * 
	 * @param accessToken
	 * @return
	 */
	public static String getSendUrl(String accessToken) {
		String returnUrl = url + "/message/mass/send";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}

	/**
	 * 获取删除群发url
	 * 
	 * @param accessToken
	 * @return
	 */
	public static String getDeleteUrl(String accessToken) {
		String returnUrl = url + "/message/mass/delete";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}

	/**
	 * 获取发送模板信息url
	 * 
	 * @param accessToken
	 * @return
	 */
	public static String getSendTemlateMsgUrl(String accessToken) {
		String returnUrl = url + "/message/template/send";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}

	/**
	 * 获取创建分组url
	 * 
	 * @param accessToken
	 * @return
	 */
	public static String getCreateGroupUrl(String accessToken) {
		String returnUrl = url + "/groups/create";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}

	/**
	 * 获取查询分组url
	 * 
	 * @param accessToken
	 * @return
	 */
	public static String getGetGroupUrl(String accessToken) {
		String returnUrl = url + "/groups/get";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}

	/**
	 * 获取根据openid查询所在分组url
	 * 
	 * @param accessToken
	 * @return
	 */
	public static String getGetIdGroupUrl(String accessToken) {
		String returnUrl = url + "/groups/getid";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}

	/**
	 * 获取修改分组url
	 * 
	 * @param accessToken
	 * @return
	 */
	public static String getUpdateGroupUrl(String accessToken) {
		String returnUrl = url + "/groups/update";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}

	/**
	 * 获取移动用户分组url
	 * 
	 * @param accessToken
	 * @return
	 */
	public static String getUpdateMemberGroupUrl(String accessToken) {
		String returnUrl = url + "/groups/members/update";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}

	/**
	 * 获取修改用户备注url
	 * 
	 * @param accessToken
	 * @return
	 */
	public static String getUpdateRemarkUrl(String accessToken) {
		String returnUrl = url + "/user/info/updateremark";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}

	/**
	 * 获取用户信息的URL
	 * 
	 * @param url
	 * @param accessToken
	 * @param openid
	 * @param lang
	 * @return
	 */
	public static String getGetUserInfoUrl(String accessToken, String openid,
			WXLang lang) {
		String returnUrl = url + "/user/info";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		returnUrl = addGetParam(returnUrl, WXUrlName.OPEN_ID, openid);
		returnUrl = addGetParam(returnUrl, WXUrlName.LANG, lang.toString());
		return returnUrl;
	}

	/**
	 * 获取关注用户openid列表
	 * 
	 * @param accessToken
	 * @param nextOpenid
	 * @return
	 */
	public static String getGetUserOpenidsUrl(String accessToken, String nextOpenid) {
		String returnUrl = url + "/user/get";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		returnUrl = addGetParam(returnUrl, WXUrlName.NEXT_OPENID, nextOpenid);
		return returnUrl;
	}

	/**
	 * 获取oauth2.0认证的code的url。
	 * 授权回调域名配置规范为全域名，比如需要网页授权的域名为：www.qq.com，配置以后此域名下面的页面http
	 * ://www.qq.com/music.html 、 http://www.qq.com/login.html
	 * 都可以进行OAuth2.0鉴权。但http://pay.qq.com 、 http://music.qq.com 、 http://qq.com
	 * 无法进行OAuth2.0鉴权。
	 * 
	 * code作为换取access_token的票据，每次用户授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期。
	 * @param appid
	 *            公众号的唯一标识
	 * @param returnUrl 授权后重定向的回调链接地址
	 * @param scope
	 * @param state 重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值
	 * @return
	 */
	public static String getGetCodeUrl(String appid, String returnUrl, WXOauthScope scope, String state) {
		String url = null;
		try {
			url = codeUrl;
			url = addGetParam(url, WXUrlName.APP_ID, appid);
			url = addGetParam(url, WXUrlName.REDIRECT_URI, URLEncoder.encode(returnUrl,"UTF-8"));
			url = addGetParam(url, WXUrlName.RESPONSE_TYPE, "code");
			url = addGetParam(url, WXUrlName.SCOPE, scope.toString());
			url = addGetParam(url, WXUrlName.STATE, state);
			url += WXUrlName.WECHAT_REDIRECT;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return url;
	}
	
	/**
	 * 获取获取oauth2.0 accessToken 的url
	 * @param appid
	 * @param secret
	 * @param code
	 * @return
	 */
	public static String getGetOauthAccessTokenUrl( String appid,String secret,String code) {
		String returnUrl = oauthUrl+"/oauth2/access_token";
		returnUrl = addGetParam(returnUrl, WXUrlName.APP_ID, appid);
		returnUrl = addGetParam(returnUrl, WXUrlName.APP_SECRET, secret);
		returnUrl = addGetParam(returnUrl, WXUrlName.CODE, code);
		returnUrl = addGetParam(returnUrl, WXUrlName.GRANT_TYPE,
				WXValue.AUTHORIZATION_CODE);
		return returnUrl;
	}
	/**
	 * 
	 * @param refreshToken
	 * @param appid
	 * @return
	 */
	public static String getRefreshOauthAccessTokenUrl(String refreshToken,String appid) {
		String returnUrl = oauthUrl+"/oauth2/refresh_token";
		returnUrl = addGetParam(returnUrl, WXUrlName.APP_ID, appid);
		returnUrl = addGetParam(returnUrl, WXUrlName.REFRESH_TOKEN, refreshToken);
		returnUrl = addGetParam(returnUrl, WXUrlName.GRANT_TYPE, WXValue.REFRESH_TOKEN);
		return returnUrl;
	}
	/**
	 * 获取检查oauth accessToken是否有效url
	 * @param accessToken
	 * @param openid
	 * @return
	 */
	public static String getCheckOauthAccessTokenUrl(String accessToken,String openid){
		String returnUrl = oauthUrl+"/auth";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		returnUrl = addGetParam(returnUrl, WXUrlName.OPEN_ID, openid);
		return returnUrl;
	}
	/**
	 * 获取oauth用户信息(需scope为 snsapi_userinfo)
	 * @param accessToken
	 * @param openid
	 * @param lang
	 * @return
	 */
	public static String getGetOauthUserInfoUrl(String accessToken,String openid,WXLang lang){
		String returnUrl = oauthUrl+"/userinfo";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		returnUrl = addGetParam(returnUrl, WXUrlName.OPEN_ID, openid);
		returnUrl = addGetParam(returnUrl, WXUrlName.LANG, lang.toString());
		return returnUrl;
	}
	/**
	 * 获取创建菜单URL
	 * @param accessToken
	 * @return
	 */
	public static String getCreateMenuUrl(String accessToken){
		String returnUrl = url+"/menu/create";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}
	/**
	 * 获取获取菜单的url
	 * @param accessToken
	 * @return
	 */
	public static String getGetMenuUrl(String accessToken){
		String returnUrl = url+"/menu/get";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}
	/**
	 * 获取删除菜单的url
	 * @param accessToken
	 * @return
	 */
	public static String getDeleteMenuUrl(String accessToken){
		String returnUrl = url+"/menu/delete";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}
	/**
	 * 获取生成二维码的url
	 * @param accessToken
	 * @return
	 */
	public static String getCreateQRcodeUrl(String accessToken){
		String returnUrl = url+"/qrcode/create";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}
	/**
	 * 获取长连接转短连接url
	 * @param accessToken
	 * @return
	 */
	public static String getShortUrlUrl(String accessToken){
		String returnUrl = url+"/shorturl";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}
	/**
	 * 获取获取客服聊天记录Url
	 * @param accessToken
	 * @return
	 */
	public static String getCustomServiceRecordUrl(String accessToken){
		String returnUrl = url+"/customservice/getrecord";
		returnUrl = addGetParam(returnUrl, WXUrlName.ACCESS_TOKEN, accessToken);
		return returnUrl;
	}
	/**
	 * 
	 * @param ticket
	 * @return
	 */
	public static String getGetQRcodeByTicketUrl(String ticket){
		String returnUrl = url+"/showqrcode";
		try {
			returnUrl = addGetParam(returnUrl, WXUrlName.TICKET, URLEncoder.encode(ticket,"UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return returnUrl;
	}

	/**
	 * 在url末端添加参数
	 * 
	 * @param url
	 * @param key
	 * @param value
	 * @return
	 */
	private static String addGetParam(String url, String key, String value) {
		String returnUrl = url == null ? "" : url;
		if (url.indexOf("?") > 0) {
			returnUrl += "&";
		} else {
			returnUrl += "?";
		}
		return returnUrl + key + "=" + value;
	}
}
