package com.zzb.weixin.exception;

import com.zzb.weixin.constant.Constant;
import com.zzb.weixin.constant.WXJsonName;
import com.zzb.weixin.json.JsonObject;
/**
 * 微信错误信息类
 * @author zzb
 *
 */
public class WeiXinException extends Exception {
	private static final long serialVersionUID = 6856196348729709780L;
	private String errCode ;
	private String errMsg;
	public WeiXinException(JsonObject jo) {
		errCode = jo.getAsString(WXJsonName.ERR_CODE);
		errMsg = jo.getAsString(WXJsonName.ERR_MSG);
	}
	public WeiXinException(String errCode,String errMsg) {
		this.errCode = errCode;
		this.errMsg = errMsg;
	}
	public String getErrCode() {
		return errCode;
	}
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	/**
	 * 存在中文的错误信息返回中文,不存在返回英文
	 * @return
	 */
	public String getErrMsgChina(){
		if(Constant.errorCodeToMsg.containsKey(errCode))
			return Constant.errorCodeToMsg.get(errCode);
		return errMsg;
	}
	@Override
	public String toString() {
		return "{"+WXJsonName.ERR_CODE+":"+errCode+","+WXJsonName.ERR_MSG+":"+errMsg+"}";
	}
}
