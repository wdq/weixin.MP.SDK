package com.zzb.weixin.constant;

public interface WXValue {
	String GRANT_TYPE = "client_credential";
	String QR_SCENE = "QR_SCENE";
	String QR_LIMIT_SCENE = "QR_LIMIT_SCENE";
	String REFRESH_TOKEN = "refresh_token";
	String AUTHORIZATION_CODE = "authorization_code";
}
