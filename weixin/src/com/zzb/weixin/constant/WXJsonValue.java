package com.zzb.weixin.constant;

/**
 * 微信Json键名称
 * 
 * @author zzb
 * */
public interface WXJsonValue {
	String CLICK = "click";
	String VIEW = "view";
	String LONG2SHORT = "long2short";
}
