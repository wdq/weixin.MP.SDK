package com.zzb.weixin.constant;

/**
 * 微信Json键名称
 * 
 * @author zzb
 * */
public interface WXJsonName {

	String ACCESS_TOKEN = "access_token";// 获取到的凭证

	String EXPIRES_IN = "expires_in";// 凭证有效时间，单位：秒

	String ERR_CODE = "errcode";// 返回码

	String ERR_MSG = "errmsg";// 说明

	String TO_USER = "touser";

	String MSG_TYPE = "msgtype";
	String CONTENT = "content";
	String TEXT = "text";
	String MEDIA_ID = "media_id";// 媒体文件上传后，获取时的唯一标识
									// 媒体文件在后台保存时间为3天，即3天后media_id失效。
	String IMAGE = "image";// 图片（image）: 1M，支持JPG格式
	String VOICE = "voice";// 语音（voice）：2M，播放长度不超过60s，支持AMR\MP3格式

	String VIDEO = "video";// 视频（video）：10MB，支持MP4格式
	String THUMB = "thumb";// 缩略图（thumb）：64KB，支持JPG格式
	String TITLE = "title";
	String DESCRIPTION = "description";

	String MUSIC = "music";
	String MUSIC_URL = "musicurl";
	String HQ_MUSIC_URL = "hqmusicurl";
	String THUMB_MEDIA_ID = "thumb_media_id";

	String NEWS = "news";
	String ARTICLES = "articles";
	String URL = "url";
	String PIC_URL = "picurl";

	String SUBSCRIBE = "subscribe";
	String OPEN_ID = "openid";
	String NICK_NAME = "nickname";
	String SEX = "sex";
	String LANGUAGE = "language";
	String CITY = "city";
	String PROVINCE = "province";
	String COUNTRY = "country";
	String HEAD_IMG_URL = "headimgurl";
	String SUBSCRIBE_TIME = "subscribe_time";

	String CREATED_AT = "created_at";// 媒体文件上传时间戳
	String TYPE = "type";

	String TOTAL = "total";
	String COUNT = "count";
	String DATA = "data";
	String NEXT_OPENID = "next_openid";

	String NAME = "name";
	String SUB_BUTTON = "sub_button";
	String KEY = "key";
	String BUTTON = "button";
	String MENU = "menu";

	String EXPIRE_SECONDS = "expire_seconds";
	String ACTION_NAME = "action_name";
	String ACTION_INFO = "action_info";
	String SCENE_ID = "scene_id";
	String TICKET = "ticket";
	String REFRESH_TOKEN = "refresh_token";

	String QR_SCENE = "QR_SCENE";
	String QR_LIMIT_SCENE = "QR_LIMIT_SCENE";

	String SCOPE = "scope";
	String PRIVILEGE = "privilege";

	String IP_LIST = "ip_list";// 微信服务器IP地址列表

	String AUTHOR = "author";// 图文消息的作者
	String CONTENT_SOURCE_URL = "contentSourceUrl";// 图文消息的作者
	String DIGEST = "digest";// 图文消息的描述
	String SHOW_COVER_PIC = "showCoverPic";// 是否显示封面，1为显示，0为不显示

	String ACTION = "action";
	String LONG_URL = "long_url";// 长连接转短连接
	String SHORT_URL = "short_url";// 长连接转短连接

	String FILTER = "filter";// 群发消息,根据分组发送消息
	String GROUP_ID = "group_id";// 群发消息,分组id
	String TOUSER = "touser";// 群发消息,接收用户openid
	String MSG_ID = "msg_id";// 群发消息,响应 消息id
	String TEMPLATE_ID = "template_id";// 发送模板消息
	String TOP_COLOR = "topcolor";//

	String VALUE = "value";// 模板消息 note值
	String COLOR = "color";// 模板消息,note颜色
	String GROUP = "group";// 分组管理
	String GROUPS = "groups";// 分组管理,分组列表
	String GROUPID = "groupid";// 分组管理,分组列表
	String TO_GROUPID = "to_groupid";// 分组管理,移动用户分组
	String ID = "id";

	String REMARK = "remark";// 设置备注名
	String UNIONID = "unionid";// 用户信息,公众平台内唯一唯一id

	String START_TIME = "starttime";// 查询开始时间，UNIX时间戳(必填)
	String END_TIME = "endtime";// 查询结束时间，UNIX时间戳，每次查询不能跨日查询(必填)
	String PAGE_SIZE = "pagesize";// 每页大小，每页最多拉取1000条(必填)
	String PAGE_INDEX = "pageindex";// 查询第几页，从1开始(必填)
	
	String RECORD_LIST="recordlist";//响应消息列表
	String WORKER="worker";//客服账号
	String OPER_CODE="opercode";//操作ID（会话状态），具体说明见下文
	String TIME="time";//操作时间，UNIX时间戳
	
}
