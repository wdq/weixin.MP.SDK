package com.zzb.weixin.constant;

import java.util.HashMap;
import java.util.Map;

public class Constant {
	/**
	 * 微信错误信息中文
	 */
	public static Map<String, String> errorCodeToMsg = new HashMap<String, String>();
	static {
		errorCodeToMsg.put("-1", "系统繁忙");
		errorCodeToMsg.put("0", "请求成功");
		errorCodeToMsg.put("40001",
				"获取access_token时AppSecret错误，或者access_token无效");
		errorCodeToMsg.put("40002", "不合法的凭证类型");
		errorCodeToMsg.put("40003", "不合法的OpenID");
		errorCodeToMsg.put("40004", "不合法的媒体文件类型");
		errorCodeToMsg.put("40005", "不合法的文件类型");
		errorCodeToMsg.put("40006", "不合法的文件大小");
		errorCodeToMsg.put("40007", "不合法的媒体文件id");
		errorCodeToMsg.put("40008", "不合法的消息类型");
		errorCodeToMsg.put("40009", "不合法的图片文件大小");
		errorCodeToMsg.put("40010", "不合法的语音文件大小");
		errorCodeToMsg.put("40011", "不合法的视频文件大小");
		errorCodeToMsg.put("40012", "不合法的缩略图文件大小");
		errorCodeToMsg.put("40013", "不合法的APPID");
		errorCodeToMsg.put("40014", "不合法的access_token");
		errorCodeToMsg.put("40015", "不合法的菜单类型");
		errorCodeToMsg.put("40016", "不合法的按钮个数");
		errorCodeToMsg.put("40017", "不合法的按钮个数");
		errorCodeToMsg.put("40018", "不合法的按钮名字长度");
		errorCodeToMsg.put("40019", "不合法的按钮KEY长度");
		errorCodeToMsg.put("40020", "不合法的按钮URL长度");
		errorCodeToMsg.put("40021", "不合法的菜单版本号");
		errorCodeToMsg.put("40022", "不合法的子菜单级数");
		errorCodeToMsg.put("40023", "不合法的子菜单按钮个数");
		errorCodeToMsg.put("40024", "不合法的子菜单按钮类型");
		errorCodeToMsg.put("40025", "不合法的子菜单按钮名字长度");
		errorCodeToMsg.put("40026", "不合法的子菜单按钮KEY长度");
		errorCodeToMsg.put("40027", "不合法的子菜单按钮URL长度");
		errorCodeToMsg.put("40028", "不合法的自定义菜单使用用户");
		errorCodeToMsg.put("40029", "不合法的oauth_code");
		errorCodeToMsg.put("40030", "不合法的refresh_token");
		errorCodeToMsg.put("40031", "不合法的openid列表");
		errorCodeToMsg.put("40032", "不合法的openid列表长度");
		errorCodeToMsg.put("40033", "不合法的请求字符，不能包含\\uxxxx格式的字符");
		errorCodeToMsg.put("40035", "不合法的参数");
		errorCodeToMsg.put("40038", "不合法的请求格式");
		errorCodeToMsg.put("40039", "不合法的URL长度");
		errorCodeToMsg.put("40050", "不合法的分组id");
		errorCodeToMsg.put("40051", "分组名字不合法");
		errorCodeToMsg.put("41001", "缺少access_token参数");
		errorCodeToMsg.put("41002", "缺少appid参数");
		errorCodeToMsg.put("41003", "缺少refresh_token参数");
		errorCodeToMsg.put("41004", "缺少secret参数");
		errorCodeToMsg.put("41005", "缺少多媒体文件数据");
		errorCodeToMsg.put("41006", "缺少media_id参数");
		errorCodeToMsg.put("41007", "缺少子菜单数据");
		errorCodeToMsg.put("41008", "缺少oauth code");
		errorCodeToMsg.put("41009", "缺少openid");
		errorCodeToMsg.put("42001", "access_token超时");
		errorCodeToMsg.put("42002", "refresh_token超时");
		errorCodeToMsg.put("42003", "oauth_code超时");
		errorCodeToMsg.put("43001", "需要GET请求");
		errorCodeToMsg.put("43002", "需要POST请求");
		errorCodeToMsg.put("43003", "需要HTTPS请求");
		errorCodeToMsg.put("43004", "需要接收者关注");
		errorCodeToMsg.put("43005", "需要好友关系");
		errorCodeToMsg.put("44001", "多媒体文件为空");
		errorCodeToMsg.put("44002", "POST的数据包为空");
		errorCodeToMsg.put("44003", "图文消息内容为空");
		errorCodeToMsg.put("44004", "文本消息内容为空");
		errorCodeToMsg.put("45001", "多媒体文件大小超过限制");
		errorCodeToMsg.put("45002", "消息内容超过限制");
		errorCodeToMsg.put("45003", "标题字段超过限制");
		errorCodeToMsg.put("45004", "描述字段超过限制");
		errorCodeToMsg.put("45005", "链接字段超过限制");
		errorCodeToMsg.put("45006", "图片链接字段超过限制");
		errorCodeToMsg.put("45007", "语音播放时间超过限制");
		errorCodeToMsg.put("45008", "图文消息超过限制");
		errorCodeToMsg.put("45009", "接口调用超过限制");
		errorCodeToMsg.put("45010", "创建菜单个数超过限制");
		errorCodeToMsg.put("45015", "回复时间超过限制");
		errorCodeToMsg.put("45016", "系统分组，不允许修改");
		errorCodeToMsg.put("45017", "分组名字过长");
		errorCodeToMsg.put("45018", "分组数量超过上限");
		errorCodeToMsg.put("46001", "不存在媒体数据");
		errorCodeToMsg.put("46002", "不存在的菜单版本");
		errorCodeToMsg.put("46003", "不存在的菜单数据");
		errorCodeToMsg.put("46004", "不存在的用户");
		errorCodeToMsg.put("47001", "解析JSON/XML内容错误");
		errorCodeToMsg.put("48001", "api功能未授权");
		errorCodeToMsg.put("50001", "用户未授权该api");
	}
	public static Map<String, String> operCodeMap = new HashMap<String, String>();
	static {
		operCodeMap.put("1000", "创建未接入会话");
		operCodeMap.put("1001", "接入会话");
		operCodeMap.put("1002", "主动发起会话");
		operCodeMap.put("1004", "关闭会话");
		operCodeMap.put("1005", "抢接会话");
		operCodeMap.put("2001", "公众号收到消息");
		operCodeMap.put("2002", "客服发送消息");
		operCodeMap.put("2003", "客服收到消息");
	}

	/**
	 * 微信事件推送类型
	 * 
	 * @author zb.zhang
	 *
	 */
	public static enum WXEvent {
		SUBSCRIBE, // /订阅
		UNSUBSCRIBE, // 取消订阅
		SCAN, // 用户已关注时扫描二维码SCAN
		LOCATION, // 上报地理位置事件
		CLICK, // 自定义菜单事件
		VIEW, // 点击菜单跳转链接时的事件推送
		MASSSENDJOBFINISH, // 群发消息完成
		TEMPLATESENDJOBFINISH, // 模板消息发送完成
		SCANCODE_PUSH, // 扫码推事件
		SCANCODE_WAITMSG, // 扫码推事件且弹出“消息接收中”提示框
		PIC_SYSPHOTO, // 弹出系统拍照发图
		PIC_PHOTO_OR_ALBUM, // 弹出拍照或者相册发图
		PIC_WEIXIN, // 弹出微信相册发图器
		LOCATION_SELECT// 弹出地理位置选择器
	}

	/**
	 * 微信消息类型
	 * 
	 * @author zb.zhang
	 *
	 */
	public static enum WXMsgType {
		text, // 文本消息
		image, // 图片消息
		voice, // 音频消息
		video, // 视频消息
		location, // 地理位置消息
		link, // 链接消息-
		event, // 事件推送
		music, // 音乐消息-客服\被动相应
		news, // 图文消息-客服\被动相应
		transfer_customer_service// 将消息转发到多客服
	}

	/**
	 * 微信媒体文件类型 上传下载媒体文件
	 * 
	 * @author zb.zhang
	 *
	 */
	public static enum WXMediaType {
		image, // 图片（image）: 1M，支持JPG格式
		voice, // 语音（voice）：2M，播放长度不超过60s，支持AMR\MP3格式
		video, // 视频（video）：10MB，支持MP4格式
		thumb// 缩略图（thumb）：64KB，支持JPG格式
	}

	/**
	 * 微信权限分类
	 * 
	 * @author zb.zhang
	 *
	 */
	public static enum WXOauthScope {
		snsapi_base, // 不弹出授权页面，直接跳转，只能获取用户openid
		snsapi_userinfo // 弹出授权页面，可通过openid拿到昵称、性别、所在地。并且，即使在未关注的情况下，只要用户授权，也能获取其信息

	}

	/**
	 * 微信语言环境
	 * 
	 * @author zb.zhang
	 *
	 */
	public static enum WXLang {
		zh_CN, zh_TW, en
	}

	/**
	 * 微信二维码
	 * 
	 * @author zb.zhang
	 *
	 */
	public static enum WXQRActionName {
		QR_SCENE, // 临时
		QR_LIMIT_SCENE// 永久
	}

	/**
	 * 微信群发消息类型
	 * 
	 * @author zb.zhang
	 *
	 */
	public static enum WXSendMsgType {
		mpnews, // 图文消息
		text, // 文本
		voice, // 语音
		image, // 图片
		mpvideo// 视频
	}

}
