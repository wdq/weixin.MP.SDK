package com.zzb.weixin.constant;

/**
 * 微信url键名称 
 * 
 * @author zzb
 * */
public interface WXUrlName {

	String APP_ID = "appid";

	String APP_SECRET = "secret";

	String GRANT_TYPE = "grant_type";
	
	String ACCESS_TOKEN = "access_token";
	
	String OPEN_ID = "openid";
	
	String TYPE = "type";
	
	String LANG = "lang";
	
	String MEDIA_ID = "media_id";
	
	String NEXT_OPENID = "next_openid";

	String IMAGE = "image";
	String VOICE = "voice";
	String VIDEO = "video";
	String THUMB = "thumb";
	String TICKET = "ticket";
	
	String APPID = "appid";
	String REDIRECT_URI = "redirect_uri";
	String RESPONSE_TYPE = "response_type";
	String SCOPE = "scope";
	String STATE = "state";
	String WECHAT_REDIRECT = "#wechat_redirect";
	
	String CODE = "code";
	String AUTHORIZATION_CODE = "authorization_code";
	String REFRESH_TOKEN = "refresh_token";
}
