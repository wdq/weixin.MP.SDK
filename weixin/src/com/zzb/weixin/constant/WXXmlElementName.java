package com.zzb.weixin.constant;

/**
 * 微信XML节点名称 
 * 
 * @author zzb
 * */
public interface WXXmlElementName {

	/* 通用公共节点 */
	String XML = "xml";
	String TO_USER_NAME = "ToUserName";//开发者微信号
	String FROM_USER_NAME = "FromUserName";//发送方帐号（一个OpenID）
	String CREATE_TIME = "CreateTime";//消息创建时间 （整型）
	String MSG_TYPE = "MsgType";

	/* 消息ID */
	String MSG_ID = "MsgId";//消息id，64位整型

	/* 文本消息节点 */
	String CONTENT = "Content";//文本消息内容
	String FUNC_FLAG = "FuncFlag";

	String PIC_URL = "PicUrl";//图片链接

	String TITLE = "Title";//消息标题
	String DESCRIPTION = "Description";//消息描述
	String URL = "Url";//消息链接
	String MUSIC_URL = "MusicUrl";
	String HQ_MUSIC_URL = "HQMusicUrl";
	String MUSIC = "Music";

	/* 事件消息节点 */
	String EVENT = "Event";//事件类型
	String EVENT_KEY = "EventKey";//事件KEY值，qrscene_为前缀，后面为二维码的参数值
	String TICKET = "Ticket";//二维码的ticket，可用来换取二维码图片
	String LATITUDE = "Latitude";// 地理位置纬度
	String LONGITUDE = "Longitude";// 地理位置经度
	String PRECISION = "Precision";// 地理位置精度

	/* 地理位置消息 */
	String LOCATION_X = "Location_X";//地理位置纬度
	String LOCATION_Y = "Location_Y";//地理位置经度
	String SCALE = "Scale";//地图缩放大小
	String LABEL = "Label";//地理位置信息

	String ARTICLE_COUNT = "ArticleCount";

	String ARTICLES = "Articles";

	String ITEM = "item";

	/* 语音识别消息 */
	String MEDIAID = "MediaId";//图片消息媒体id，可以调用多媒体文件下载接口拉取数据。
	String FORMAT = "Format";
	String RECOGNITION = "Recognition";

	// 视频缩略图
	String THUMBMEDIAID = "ThumbMediaId";//视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。

	String IMAGE = "Image";

	String VOICE = "Voice";
	String VIDEO = "Video";

	String STATUS = "Status";
	String TOTAL_COUNT = "TotalCount";
	String FILTER_COUNT = "FilterCount";
	String SENT_COUNT = "SentCount";
	String ERROR_COUNT = "ErrorCount";
	
	String SCAN_CODE_INFO = "ScanCodeInfo";//扫描信息
	String SCAN_TYPE = "ScanType";//扫描类型，一般是qrcode
	String SCAN_RESULT = "ScanResult";//扫描结果，即二维码对应的字符串信息
	
	String SEND_PICS_INFO = "SendPicsInfo";//发送的图片信息
	String COUNT = "Count";//发送的图片数量
	String PIC_LIST = "PicList";//图片列表
	String PIC_MD5_SUM = "PicMd5Sum";//图片的MD5值，开发者若需要，可用于验证接收到图片
	
	String POI_NAME = "Poiname";//朋友圈POI的名字，可能为空
}
