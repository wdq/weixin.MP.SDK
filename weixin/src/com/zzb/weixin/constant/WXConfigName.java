package com.zzb.weixin.constant;
/**
 * 微信配置文件键
 * @author user
 *
 */
public interface WXConfigName {
	String APP_ID = "appid";
	String APP_SECRET = "appSecret";
	String BASE_URL = "baseURL";
	String MEDIA_URL = "mediaUrl";
	String CODE_URL = "codeUrl";
	String OAUTH_URL = "oauthUrl";
}
