package com.zzb.weixin;

import org.w3c.dom.Document;

import com.zzb.weixin.model.event.ClickMsg;
import com.zzb.weixin.model.event.EventMsg;
import com.zzb.weixin.model.event.LocationEventMsg;
import com.zzb.weixin.model.event.LocationSelectMsg;
import com.zzb.weixin.model.event.MsgSendJobFinishMsg;
import com.zzb.weixin.model.event.PicPhotoOrAlbumMsg;
import com.zzb.weixin.model.event.PicSysphotoMsg;
import com.zzb.weixin.model.event.PicWeixinMsg;
import com.zzb.weixin.model.event.ScanMsg;
import com.zzb.weixin.model.event.ScancodePushMsg;
import com.zzb.weixin.model.event.ScancodeWaitmsgMsg;
import com.zzb.weixin.model.event.SubscribeMsg;
import com.zzb.weixin.model.event.UnSubscribeMsg;
import com.zzb.weixin.model.event.ViewMsg;
import com.zzb.weixin.model.msg.basic.HeadMsg;
import com.zzb.weixin.model.msg.basic.ImageMsg;
import com.zzb.weixin.model.msg.basic.LinkMsg;
import com.zzb.weixin.model.msg.basic.LocationMsg;
import com.zzb.weixin.model.msg.basic.Msg;
import com.zzb.weixin.model.msg.basic.TextMsg;
import com.zzb.weixin.model.msg.basic.VideoMsg;
import com.zzb.weixin.model.msg.basic.VoiceMsg;

public class WeiXinMsgFactory {
	public static Msg getMsg(Document document) {
		Msg msg = null;
		HeadMsg head = new HeadMsg(document);
		switch (head.getMsgType()) {
		case text:
			msg = new TextMsg(head);
			msg.setDocument(document);
			break;
		case image:
			msg = new ImageMsg(head);
			msg.setDocument(document);
			break;
		case event:
			msg = getEventMsg(document, head);
			break;
		case link:
			msg = new LinkMsg(head);
			msg.setDocument(document);
			break;
		case location:
			msg = new LocationMsg(head);
			msg.setDocument(document);
			break;
		case voice:
			msg = new VoiceMsg(head);
			msg.setDocument(document);
			break;
		case video:
			msg = new VideoMsg(head);
			msg.setDocument(document);
			break;
		default:
			break;
		}
		msg.read();
		return msg;
	}

	private static EventMsg getEventMsg(Document document, HeadMsg head) {
		EventMsg msg = new EventMsg(head);
		msg.setDocument(document);
		switch (msg.getEvent()) {
		case SUBSCRIBE:
			msg = new SubscribeMsg(head);
			msg.setDocument(document);
			break;
		case UNSUBSCRIBE:
			msg = new UnSubscribeMsg(head);
			msg.setDocument(document);
			break;
		case SCAN:
			msg = new ScanMsg(head);
			msg.setDocument(document);
			break;
		case LOCATION:
			msg = new LocationEventMsg(head);
			msg.setDocument(document);
			break;
		case CLICK:
			msg = new ClickMsg(head);
			msg.setDocument(document);
			break;
		case VIEW:
			msg = new ViewMsg(head);
			msg.setDocument(document);
			break;
		case MASSSENDJOBFINISH:
			msg = new MsgSendJobFinishMsg(head);
			msg.setDocument(document);
			break;
		case SCANCODE_PUSH:
			msg = new ScancodePushMsg(head);
			msg.setDocument(document);
			break;
		case SCANCODE_WAITMSG:
			msg = new ScancodeWaitmsgMsg(head);
			msg.setDocument(document);
			break;
		case PIC_SYSPHOTO:
			msg = new PicSysphotoMsg(head);
			msg.setDocument(document);
			break;
		case PIC_PHOTO_OR_ALBUM:
			msg = new PicPhotoOrAlbumMsg(head);
			msg.setDocument(document);
			break;
		case PIC_WEIXIN:
			msg = new PicWeixinMsg(head);
			msg.setDocument(document);
			break;
		case LOCATION_SELECT:
			msg = new LocationSelectMsg(head);
			msg.setDocument(document);
			break;
		default:
			break;
		}
		return msg;
	}
}
