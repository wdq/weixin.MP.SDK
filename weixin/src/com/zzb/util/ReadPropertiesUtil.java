package com.zzb.util;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Properties;

public class ReadPropertiesUtil {
	private String fileName = null;

	public ReadPropertiesUtil(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * 读取资源文件
	 * 
	 * @param fileName
	 * @return
	 */
	public String getProperties(String key) {
		InputStream is = null;
		try {
			Properties p = new Properties();
			is = this.getClass().getClassLoader().getResourceAsStream(fileName);
			p.load(is);
			return p.getProperty(key);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	/**
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public void setProperties(String key,String value) {
		OutputStream os = null;
		try {
			os = this.getClass().getClassLoader().getResource(fileName).openConnection().getOutputStream();
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
			bw.newLine();
			bw.append(key).append("=").append(value);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (os != null) {
					os.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 读取资源文件
	 * 
	 * @param fileName
	 * @return
	 */
	public boolean containsKey(String key) {
		InputStream is = null;
		try {
			Properties p = new Properties();
			is = this.getClass().getClassLoader().getResourceAsStream(fileName);
			p.load(is);
			return p.containsKey(key);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
}
