package com.zzb.util;


public class StringUtil {
	public static String getNotNullString(String string) {
		return getNotNullString(string, "");
	}

	public static String getNotNullHTMLString(String string) {
		return getNotNullString(string, "&nbsp;");
	}

	/**
	 * 获取非空字符串，空字串由指定字符串代替
	 * 
	 * @param string
	 *            原字符串
	 * @param string2
	 *            string为空时的替换字符串
	 * @return
	 */
	public static String getNotNullString(String string, String string2) {
		if (string2 == null)
			string2 = "";
		return isEmpty(string) ? string2 : string;
	}

	public static String getSubString(String string, int index) {
		if (string != null && string.length() > index) {
			return string.substring(0, index) + "..";
		} else {
			return getNotNullString(string);
		}
	}

	public static String getSubHTMLString(String string, int index) {
		if (string != null && string.length() > index) {
			return "<span title='" + string + "'>" + string.substring(0, index)
					+ "..</span>";
		} else {
			return getNotNullHTMLString(string);
		}
	}

	public static boolean isEmpty(String string) {
		if (string == null || string.equals("")
				|| string.toLowerCase().equals("null")
				|| string.toLowerCase().equals("undefined"))
			return true;
		else
			return false;
	}
}
