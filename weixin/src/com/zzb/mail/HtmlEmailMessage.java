package com.zzb.mail;

public class HtmlEmailMessage extends EmailMessage {
	private String content;
	private String contentCharSet;

	public HtmlEmailMessage(String sendTo, String sendCC, String sendBCC,
			String subject, String content, String contentCharSet,
			String attachments, boolean is_url) {
		this.setSendTo(sendTo);
		this.setSendCC(sendCC);
		this.setSendBCC(sendBCC);
		this.setSubject(subject);
		this.setContent(content);
		this.setContentCharSet(contentCharSet);
		this.setAttachments(attachments);
		this.setIs_url(is_url);
	}

	public HtmlEmailMessage(String sendTo, String subject, String content) {
		this.setSendTo(sendTo);
		this.setSubject(subject);
		this.setContent(content);
		this.setContentCharSet("utf-8");
	}

	public HtmlEmailMessage(String sendTo, String subject, String content,
			String contentCharSet) {
		this.setSendTo(sendTo);
		this.setSubject(subject);
		this.setContent(content);
		this.setContentCharSet(contentCharSet);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContentCharSet() {
		return contentCharSet;
	}

	public void setContentCharSet(String contentCharSet) {
		this.contentCharSet = contentCharSet;
	}
}
