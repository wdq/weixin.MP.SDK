package com.zzb.mail;

public class EmailReciveInfo {
	private String userName;
	private String password;
	private String imapServer;
	private int imapPort;
	private boolean isSSL;

	/**
	 * 
	 * @param userName
	 *            用户名
	 * @param password
	 *            密码
	 * @param imapServer
	 *            imap服务器
	 * @param imapPort
	 *            imap端口
	 */
	public EmailReciveInfo(String userName, String password, String imapServer,
			int imapPort) {
		this.setUserName(userName);
		this.setPassword(password);
		this.setImapServer(imapServer);
		this.setImapPort(imapPort);

	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getImapServer() {
		return imapServer;
	}

	public void setImapServer(String imapServer) {
		this.imapServer = imapServer;
	}

	public int getImapPort() {
		return imapPort;
	}

	public void setImapPort(int imapPort) {
		this.imapPort = imapPort;
	}

	public boolean isSSL() {
		return isSSL;
	}

	public void setSSL(boolean isSSL) {
		this.isSSL = isSSL;
	}
}
