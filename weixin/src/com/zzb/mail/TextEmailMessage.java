package com.zzb.mail;

public class TextEmailMessage extends EmailMessage {
	private String content;

	public TextEmailMessage(String sendTo, String sendCC, String sendBCC,
			String subject, String content, String attachments, boolean is_url) {
		this.setSendTo(sendTo);
		this.setSendCC(sendCC);
		this.setSendBCC(sendBCC);
		this.setSubject(subject);
		this.setContent(content);
		this.setAttachments(attachments);
		this.setIs_url(is_url);
	}

	public TextEmailMessage(String sendTo, String subject, String content) {
		this.setSendTo(sendTo);
		this.setSubject(subject);
		this.setContent(content);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
