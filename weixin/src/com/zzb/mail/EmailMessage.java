package com.zzb.mail;

import com.zzb.util.ReadPropertiesUtil;
import com.zzb.util.StringUtil;

public abstract class EmailMessage {
	private String sendTo;
	private String sendCC;
	private String sendBCC;
	private ReadPropertiesUtil rdp = new ReadPropertiesUtil("email.properties");
	private String subject = StringUtil.getNotNullString(
			rdp.getProperties("defalt.subject"), "empty subject");
	private String attachments;
	private boolean is_url;

	public String getSendTo() {
		return sendTo;
	}

	/**
	 * 设置收件人，","分割的email数组
	 * 
	 * @param sendTo
	 */
	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}

	public String getSendCC() {
		return sendCC;
	}

	/**
	 * 设置抄送，","分割的email数组
	 * 
	 * @param sendCC
	 */
	public void setSendCC(String sendCC) {
		this.sendCC = sendCC;
	}

	public String getSendBCC() {
		return sendBCC;
	}

	/**
	 * 设置密送，","分割的email数组
	 * 
	 * @param sendCC
	 */
	public void setSendBCC(String sendBCC) {
		this.sendBCC = sendBCC;
	}

	public String getSubject() {
		return subject;
	}

	/**
	 * 设置邮件主题
	 * 
	 * @param subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getAttachments() {
		return attachments;
	}

	/**
	 * 设置附件，","分割的附件path数组
	 * 
	 * @param attachments
	 */
	public void setAttachments(String attachments) {
		this.attachments = attachments;
	}

	public boolean isIs_url() {
		return is_url;
	}

	/**
	 * 指明附件是否为url
	 * 
	 * @param is_url
	 */
	public void setIs_url(boolean is_url) {
		this.is_url = is_url;
	}
}
