package com.zzb.mail;

public class EmailSendInfo {
	private String mailServerHost;
	private String mailServerPort;
	private String userName;
	private String password;
	private boolean isSSL;

	public EmailSendInfo() {
	}

	public EmailSendInfo(String userName, String password) {
		this(userName, password, "", "");
	}

	public EmailSendInfo(String userName, String password,
			String mailServerHost, String mailServerPort) {
		this(userName, password, mailServerHost, mailServerPort, false);
	}
		
		public EmailSendInfo(String userName, String password,
				String mailServerHost, String mailServerPort,boolean isSSL) {
		this.userName = userName;
		this.password = password;
		this.mailServerHost = mailServerHost;
		this.mailServerPort = mailServerPort;
		this.isSSL = isSSL;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMailServerHost() {
		return mailServerHost;
	}

	public void setMailServerHost(String mailServerHost) {
		this.mailServerHost = mailServerHost;
	}

	public String getMailServerPort() {
		return mailServerPort;
	}

	public void setMailServerPort(String mailServerPort) {
		this.mailServerPort = mailServerPort;
	}

	public boolean isSSL() {
		return isSSL;
	}

	public void setSSL(boolean isSSL) {
		this.isSSL = isSSL;
	}
}