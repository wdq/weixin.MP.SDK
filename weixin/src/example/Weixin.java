package example;

import java.util.ArrayList;
import java.util.List;

import com.zzb.weixin.WeiXinUtil;
import com.zzb.weixin.constant.Constant.WXEvent;
import com.zzb.weixin.constant.Constant.WXLang;
import com.zzb.weixin.exception.WeiXinException;
import com.zzb.weixin.listener.DefaultHandleMessageListener;
import com.zzb.weixin.listener.DefaultListenerManager;
import com.zzb.weixin.model.AccessToken;
import com.zzb.weixin.model.event.EventMsg;
import com.zzb.weixin.model.menu.Menu;
import com.zzb.weixin.model.menu.MenuItem;
import com.zzb.weixin.model.msg.basic.ImageMsg;
import com.zzb.weixin.model.msg.basic.ImageTextData;
import com.zzb.weixin.model.msg.basic.ImageTextMsg;
import com.zzb.weixin.model.msg.basic.TextMsg;
import com.zzb.weixin.model.msg.basic.VoiceMsg;
import com.zzb.weixin.model.user.WeiXinUser;

public class Weixin {
	/**
	 * 获取accessToken示例
	 * @return
	 */
	public AccessToken getAccessToken() {
		AccessToken accessToken = null;
		try {
			//读取config.properties的配置，如指定会覆盖config.properties的配置，不会修改配置文件
			accessToken = WeiXinUtil.getAccessToken();
			System.out.println(accessToken.getAccessToken());
			System.out.println(accessToken.getExpiresIn());
		} catch (WeiXinException e) {
			e.printStackTrace();
		}
		return accessToken;
	}
	/**
	 * 发送文本信息示例
	 */
	public void sendCustonMsg() {// OK
		String openid = "openid";//微信针对该平台的用户的加密后的openid
		String msg = "msg";
		TextMsg textMsg = new TextMsg();
		textMsg.setToUserName(openid);
		textMsg.setContent(msg);
		String accessToken = getAccessToken().getAccessToken();//token最好缓存在session内，有效期较长，且有调用次数限制
		try {
			WeiXinUtil.sendMsg(accessToken, textMsg);
		} catch (WeiXinException e) {
		}
	}
	/**
	 * 获取用户信息示例
	 */
	public void user() {// OK
		String openid = "openid";//微信针对该平台的用户的加密后的openid
		String accessToken = getAccessToken().getAccessToken();//token最好缓存在session内，有效期较长，且有调用次数限制
		WeiXinUser user = null;
		try {
			user = WeiXinUtil.getWeiXinUser(accessToken, openid,WXLang.zh_CN);
		} catch (WeiXinException e) {
			e.printStackTrace();
		}
		System.out.println(user);
	}
	/**
	 * 微信检查授权示例
	 * 当微信url配置到自己的项目时访问该方法校验该网站是否是用户的网站
	 */
	public void check() {
		final String TOKEN = "3nwk9AihFYMMClSB";//令牌，用户输入
		String signature = "signature";// 微信加密签名，微信访问时的数据
		String timestamp = "timestamp";// 时间戳，微信访问时的数据
		String nonce = "nonce";// 随机数，微信访问时的数据
		String echostr = "echostr";// 随机字符串，微信访问时的数据

		if (WeiXinUtil.check(TOKEN, timestamp, nonce, signature)) {
			//echostr;// 请求验证成功，返回随机码，ajax响应方式
			System.out.println(echostr);
		} else {
		}
	}
	public static void main(String[] args) {
		new Weixin().menu();
	}
	
	public void menu(){
		Menu menu = new Menu();
		MenuItem menuItem = new MenuItem();
		menuItem.setKey("key");
		menuItem.setName("测试");
		menuItem.setType(WXEvent.CLICK);
 		menu.addButton(menuItem);
 		
 		
 		List<MenuItem> childrenButton = new ArrayList<MenuItem>();
 		
		MenuItem child1 = new MenuItem();
		child1.setType(WXEvent.CLICK);
		child1.setKey("keyChild1");
		child1.setName("测试子1");
		childrenButton.add(child1);
 		
 		MenuItem button = new MenuItem();
 		button.setName("按钮测试");
 		button.setSubButton(childrenButton);
 		menu.addButton(button);
		
		try {
			WeiXinUtil.createMenu( getAccessToken().getAccessToken(), menu);
		} catch (WeiXinException e) {
			e.printStackTrace();
		}
	}
	
	
	//以下代码为处理微信接收的消息并自动返回指定内容的代码，具体实现内容可自行修改
	private static DefaultListenerManager listenerManger = DefaultListenerManager
			.newInstance();
	static {
		//向监听器中添加事件响应方法，可以添加多个，会遍历执行
		listenerManger.addOnHandleMessageListener(new DefaultHandleMessageListener() {

			@Override
			public boolean onMsg(VoiceMsg msg) {
				TextMsg reMsg = new TextMsg();
				reMsg.setFromUserName(msg.getToUserName());
				reMsg.setToUserName(msg.getFromUserName());
				reMsg.setCreateTime(msg.getCreateTime());
				reMsg.setContent("识别结果: " + msg.getRecognition());
				listenerManger.callback(reMsg);// 回传消息
				return true;
			}

			@Override
			public boolean onMsg(TextMsg msg) {
				if ("1".equals(msg.getContent())) {// 菜单选项1
					System.out.println(msg.getFromUserName());
					TextMsg reMsg = new TextMsg();
					reMsg.setFromUserName(msg.getToUserName());
					reMsg.setToUserName(msg.getFromUserName());
					reMsg.setCreateTime(msg.getCreateTime());
					reMsg.setContent("【菜单】\n" + "1. 功能菜单\n" + "2. 图文消息测试\n"
							+ "3. 图片消息测试\n");
					System.out.println(msg.getFromUserName());
					listenerManger.callback(reMsg);// 回传消息
				} else if ("2".equals(msg.getContent())) {
					// 回复一条消息
					ImageTextData d1 = new ImageTextData(
							"测试",
							"测试",
							"http://zhzhbin1030.vicp.cc/public/images/logo.png",
							"http://zhzhbin1030.vicp.cc/new/overview");
					ImageTextData d2 = new ImageTextData(
							"测试",
							"测试",
							"http://zhzhbin1030.vicp.cc/public/images/logo.png",
							"http://zhzhbin1030.vicp.cc/new/overview");

					ImageTextMsg mit = new ImageTextMsg();
					mit.setFromUserName(msg.getToUserName());
					mit.setToUserName(msg.getFromUserName());
					mit.setCreateTime(msg.getCreateTime());
					try {
						mit.addItem(d1);
						mit.addItem(d2);
					} catch (Exception e) {
						e.printStackTrace();
					}
					listenerManger.callback(mit);
				} else if ("3".equals(msg.getContent())) {
					ImageMsg rmsg = new ImageMsg();
					rmsg.setFromUserName(msg.getToUserName());
					rmsg.setToUserName(msg.getFromUserName());
					rmsg.setPicUrl("http://zhzhbin1030.vicp.cc/public/images/logo.png");
					listenerManger.callback(rmsg);
				} else if ("4".equals(msg.getContent())) {
					TextMsg reMsg = new TextMsg();
					reMsg.setFromUserName(msg.getToUserName());
					reMsg.setToUserName(msg.getFromUserName());
					reMsg.setCreateTime(msg.getCreateTime());
					reMsg.setContent("<a href=\"http://mp.weixin.qq.com/mp/getmasssendmsg?__biz=MzA3MzQyMDkxNQ==#wechat_webview_type=1&wechat_redirect\">查看历史消息</a>");
					System.out.println(msg.getFromUserName());
					listenerManger.callback(reMsg);// 回传消息
				} else {
					TextMsg reMsg = new TextMsg();
					reMsg.setFromUserName(msg.getToUserName());
					reMsg.setToUserName(msg.getFromUserName());
					reMsg.setCreateTime(msg.getCreateTime());

					reMsg.setContent("消息命令未支持，谢谢您的支持！");

					listenerManger.callback(reMsg);// 回传消息
				}
				return true;
			}

			@Override
			public boolean onMsg(EventMsg msg) {
				switch (msg.getEvent()) {
				case SUBSCRIBE:// 订阅
					System.out.println("关注人：" + msg.getFromUserName());
					System.out.println("参数值：" + msg.getEventKey());

					TextMsg reMsg = new TextMsg();
					reMsg.setFromUserName(msg.getToUserName());
					reMsg.setToUserName(msg.getFromUserName());
					reMsg.setCreateTime(msg.getCreateTime());

					reMsg.setContent("【菜单】\n" + "1. 功能菜单\n" + "2. 图文消息测试\n"
							+ "3. 图片消息测试\n");

					listenerManger.callback(reMsg);// 回传消息
					return true;
					
				case UNSUBSCRIBE:// 取消订阅
					System.out.println("取消关注：" + msg.getFromUserName());
					return true;
				case CLICK:// 点击事件
					System.out.println("用户：" + msg.getFromUserName());
					System.out.println("点击Key：" + msg.getEventKey());
					TextMsg reMsg2 = new TextMsg();
					reMsg2.setFromUserName(msg.getToUserName());
					reMsg2.setToUserName(msg.getFromUserName());
					reMsg2.setCreateTime(msg.getCreateTime());

					reMsg2.setContent("【菜单】\n" + "1. 功能菜单\n" + "2. 图文消息测试\n"
							+ "3. 图片消息测试\n");

					listenerManger.callback(reMsg2);// 回传消息
					return true;
				default:
					return false;
				}
			}
		});
	}

}
